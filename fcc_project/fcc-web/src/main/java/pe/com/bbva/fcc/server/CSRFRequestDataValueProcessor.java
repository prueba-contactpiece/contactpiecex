package pe.com.bbva.fcc.server;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.support.RequestDataValueProcessor;

public class CSRFRequestDataValueProcessor implements RequestDataValueProcessor {

	@Override
	public Map<String, String> getExtraHiddenFields(HttpServletRequest httpServletRequest) {
		Map<String, String> hiddenFields = new HashMap<String, String>();
		hiddenFields.put(CSRFTokenManager.CSRF_PARAM_NAME, CSRFTokenManager.getTokenForSession(httpServletRequest.getSession()));
		return hiddenFields;
	}
	
	@Override
	public String processAction(HttpServletRequest httpServletRequest, String action1, String action2) {
		return action1;
	}

	@Override
	public String processFormFieldValue(HttpServletRequest httpServletRequest, String name, String value, String type) {
		return value;
	}

	@Override
	public String processUrl(HttpServletRequest httpServletRequest, String url) {
		return url;
	}
}
