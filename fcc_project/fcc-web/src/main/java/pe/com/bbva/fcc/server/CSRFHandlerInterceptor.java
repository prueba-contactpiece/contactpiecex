package pe.com.bbva.fcc.server;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class CSRFHandlerInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (!request.getMethod().equalsIgnoreCase(HttpMethod.POST.toString())) {
			return true;
		} else {
			String sessionToken = CSRFTokenManager.getTokenForSession(request.getSession());
			String requestToken = CSRFTokenManager.getTokenFromRequest(request);			
			if (StringUtils.equals(sessionToken, requestToken)) {
				return true;
			} else {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "CSRF error no acceso");
				return true;
			}
		}
	}	
}
