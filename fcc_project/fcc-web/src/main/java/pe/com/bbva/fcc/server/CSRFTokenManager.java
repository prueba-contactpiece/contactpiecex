package pe.com.bbva.fcc.server;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CSRFTokenManager {
	
	public static final String CSRF_PARAM_NAME = "X-CSRF-TOKEN";
	public final static String CSRF_TOKEN_FOR_SESSION_ATTR_NAME = CSRFTokenManager.class.getName() + ".tokenval";

	public static String getTokenForSession(HttpSession session) {
		String token = null;
		synchronized (session) {
			token = (String) session.getAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME);
			if (null == token) {
				token = UUID.randomUUID().toString();
				session.setAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME, token);
			}
		}
		return token;
	}
	
	public static String getTokenFromRequest(HttpServletRequest request) {
		return request.getHeader(CSRF_PARAM_NAME);
	}

	private CSRFTokenManager() {};
}
