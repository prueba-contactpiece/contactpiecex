package pe.com.bbva.fcc.dominio.zic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PlasticLifeCycle {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected OptionsList type;
	public OptionsList getType() { return type; }
	public void setType(OptionsList type) { this.type = type; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date date; 
	public Date getDate() { return date; }
	public void setDate(Date date) { this.date = date; }
	
}
