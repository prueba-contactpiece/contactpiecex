
package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class Amount {
	protected String currency;
    protected Double amount;

    /**
     * Obtiene el valor de la propiedad currency
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Define el valor de la propiedad currency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Obtiene el valor de la propiedad amount
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
