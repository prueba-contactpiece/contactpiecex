package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Condition {

	protected List<ItemCondition> items;
	public List<ItemCondition> getItems() { return items; }
	public void setItems(List<ItemCondition> items) { this.items = items; }
	
	protected List<FormatCondition> formats;
	public List<FormatCondition> getFormats() { return formats; }
	public void setFormats(List<FormatCondition> formats) { this.formats = formats; }
	
}