package pe.com.bbva.fcc.dominio.serv.contrato;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Interest {

	protected BigDecimal percentage;
	public BigDecimal getPercentage() { return percentage; }
	public void setPercentage(BigDecimal percentage) { this.percentage = percentage; }
	
	protected OptionsList type;
	public OptionsList getType() { return type; }
	public void setType(OptionsList type) { this.type = type; }		
}