package pe.com.bbva.fcc.dominio.serv.tarjeta;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class Limit {

	protected String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	protected Amount disposedBalance;
	public Amount getDisposedBalance() { return disposedBalance; }
	public void setDisposedBalance(Amount disposedBalance) { this.disposedBalance = disposedBalance; }

	protected Amount availableBalance;
	public Amount getAvailableBalance() { return availableBalance; }
	public void setAvailableBalance(Amount availableBalance) { this.availableBalance = availableBalance; }
	
	protected Amount currentValue;
	public Amount getCurrentValue() { return currentValue; }
	public void setCurrentValue(Amount currentValue) { this.currentValue = currentValue; }
	
	protected Amount disposedBalanceLocalCurrency;
	public Amount getDisposedBalanceLocalCurrency() { return disposedBalanceLocalCurrency; }
	public void setDisposedBalanceLocalCurrency(Amount disposedBalanceLocalCurrency) { this.disposedBalanceLocalCurrency = disposedBalanceLocalCurrency; }
	
	protected Amount availableBalanceLocalCurrency;
	public Amount getAvailableBalanceLocalCurrency() { return availableBalanceLocalCurrency; }
	public void setAvailableBalanceLocalCurrency(Amount availableBalanceLocalCurrency) { this.availableBalanceLocalCurrency = availableBalanceLocalCurrency; }
	
	protected Amount financingDisposedBalance;
	public Amount getFinancingDisposedBalance() { return financingDisposedBalance; }
	public void setFinancingDisposedBalance(Amount financingDisposedBalance) { this.financingDisposedBalance = financingDisposedBalance; }
	
}