package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DelinquencyIneligibleData {
	
	protected String id;
	protected String description;
	protected OptionsList type;
	
	/**
     * Obtiene el valor de la propiedad id.
     * @return
     *     possible object is
     *     {@link String }
     */
	public String getId() {
		return id;
	}
	/**
     * Define el valor de la propiedad id.
     * @param value
     *     allowed object is
     *     {@link String }
     */
	public void setId(String id) {
		this.id = id;
	}
	/**
     * Obtiene el valor de la propiedad description.
     * @return
     *     possible object is
     *     {@link String }
     */
	public String getDescription() {
		return description;
	}
	/**
     * Define el valor de la propiedad description.
     * @param value
     *     allowed object is
     *     {@link String }
     */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
     * Obtiene el valor de la propiedad type.
     * @return
     *     possible object is
     *     {@link OptionsList }
     */
	public OptionsList getType() {
		return type;
	}
	/**
     * Define el valor de la propiedad type.
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     */
	public void setType(OptionsList type) {
		this.type = type;
	}
	
}
