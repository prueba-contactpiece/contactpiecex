
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Job {

    protected String id;
    protected Company company;
    protected Date jobSeniority;
    protected String jobTitle;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad company.
     * 
     * @return
     *     possible object is
     *     {@link Company }
     *     
     */
    public Company getCompany() {
        return company;
    }

    /**
     * Define el valor de la propiedad company.
     * 
     * @param value
     *     allowed object is
     *     {@link Company }
     *     
     */
    public void setCompany(Company value) {
        this.company = value;
    }

    /**
     * Obtiene el valor de la propiedad jobSeniority.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getJobSeniority() {
        return jobSeniority;
    }

    /**
     * Define el valor de la propiedad jobSeniority.
     * 
     * @param value
     *     allowed object is
     *     {@link Date }
     *     
     */
    public void setJobSeniority(Date value) {
        this.jobSeniority = value;
    }

    /**
     * Obtiene el valor de la propiedad jobTitle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Define el valor de la propiedad jobTitle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

}
