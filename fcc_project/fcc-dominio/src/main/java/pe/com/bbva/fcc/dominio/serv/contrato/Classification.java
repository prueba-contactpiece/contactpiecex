
package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Classification {

    protected Segment globalSegmentation;
    protected List<Segment> segments;
    protected OptionsList riskStudyModel;

    /**
     * Obtiene el valor de la propiedad globalSegmentation.
     * 
     * @return
     *     possible object is
     *     {@link Segment }
     *     
     */
    public Segment getGlobalSegmentation() {
        return globalSegmentation;
    }

    /**
     * Define el valor de la propiedad globalSegmentation.
     * 
     * @param value
     *     allowed object is
     *     {@link Segment }
     *     
     */
    public void setGlobalSegmentation(Segment value) {
        this.globalSegmentation = value;
    }

    /**
     * Gets the value of the segments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the segments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSegments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Segment }
     * 
     * 
     */
    public List<Segment> getSegments() {
        return this.segments;
    }

    /**
     * Obtiene el valor de la propiedad riskStudyModel.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getRiskStudyModel() {
        return riskStudyModel;
    }

    /**
     * Define el valor de la propiedad riskStudyModel.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setRiskStudyModel(OptionsList value) {
        this.riskStudyModel = value;
    }

}
