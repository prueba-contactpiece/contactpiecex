package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class OperationalRestriction {
	
	protected String id;
	public String getId() {return id;}
	public void setId(String id) {this.id = id;}
	
	protected String name;
	public String getName() {return name;	}
	public void setName(String name) {this.name = name;}

	protected String isActive;
	public String getIsActive() {return isActive;}
	public void setIsActive(String isActive) {this.isActive = isActive;}
	
	
}
