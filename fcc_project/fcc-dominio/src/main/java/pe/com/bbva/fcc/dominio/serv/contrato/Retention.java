package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Retention {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money currentAmount;
	public Money getCurrentAmount() { return currentAmount; }
	public void setCurrentAmount(Money currentAmount) { this.currentAmount = currentAmount; }

}
