package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class RelatedContract {
	
	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }

}
