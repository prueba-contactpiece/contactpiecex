package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PlasticLifeCycle {
	
	protected Type type;
	public Type getType() {	return type; }
	public void setType(Type type) { this.type = type; }
	
	protected String date;
	public String getDate() { return date; }
	public void setDate(String date) { this.date = date; }
	
}
