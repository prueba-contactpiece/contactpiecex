package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

public class DelinquencyContract {
	
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date  expirationDate;
	public Date getExpirationDate() { return expirationDate; }
	public void setExpirationDate(Date expirationDate) { this.expirationDate = expirationDate; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money capitalAmount;
	public Money getCapitalAmount() { return capitalAmount; }
    public void setCapitalAmount(Money capitalAmount) { this.capitalAmount = capitalAmount; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money outstandingDebt;
	public Money getOutstandingDebt() { return outstandingDebt; }
    public void setOutstandingDebt(Money outstandingDebt) { this.outstandingDebt = outstandingDebt; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  startExpirationDate;
	public String getStartExpirationDate() { return startExpirationDate; }
	public void setStartExpirationDate(String startExpirationDate) { this.startExpirationDate = startExpirationDate; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money capitalDelinquencyBalance;
	public Money getCapitalDelinquencyBalance() { return capitalDelinquencyBalance; }
	public void setCapitalDelinquencyBalance(Money capitalDelinquencyBalance) { this.capitalDelinquencyBalance = capitalDelinquencyBalance; }
		
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money delinquencyAmount;
	public Money getDelinquencyAmount() { return delinquencyAmount; }
	public void setDelinquencyAmount(Money delinquencyAmount) { this.delinquencyAmount = delinquencyAmount; }



}
