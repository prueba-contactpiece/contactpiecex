package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonInclude;

public class User {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String alias;
	public String getAlias() { return alias; }
	public void setAlias(String alias) { this.alias = alias; }

}
