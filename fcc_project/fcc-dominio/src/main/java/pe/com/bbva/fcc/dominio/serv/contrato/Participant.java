package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;
import pe.com.bbva.fcc.dominio.serv.Pagination;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Participant {	
	
	protected Relationship relationship;
	public Relationship getRelationship() { return relationship; }
	public void setRelationship(Relationship relationship) { this.relationship = relationship; }

	protected List<Person> items;
	public List<Person> getItems() { return items; }
	public void setItems(List<Person> items) { this.items = items; }
	
	protected Pagination pagination;
	public Pagination getPagination() { return pagination; }
	public void setPagination(Pagination pagination) { this.pagination = pagination; }
		
}