package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Installments {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money installmentAmount;
	public Money getInstallmentAmount() { return installmentAmount; }
	public void setInstallmentAmount(Money installmentAmount) { this.installmentAmount = installmentAmount; }
	
}
