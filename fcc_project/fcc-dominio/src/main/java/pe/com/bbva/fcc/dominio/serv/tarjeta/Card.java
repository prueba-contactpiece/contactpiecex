package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown=true)

public class Card {

	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	protected Formats formats;
	public Formats getFormats() { return formats; }
	public void setFormats(Formats formats) { this.formats = formats; }
	
	protected Currency currency;
	public Currency getCurrency() {	return currency; }
	public void setCurrency(Currency currency) { this.currency = currency; }

	protected String expirationDate;
	public String getExpirationDate() { return expirationDate; }
	public void setExpirationDate(String expirationDate) { this.expirationDate = expirationDate; } 
	
	protected List<Participant> participants;
	public List<Participant> getParticipants() { return participants; }
	public void setParticipants(List<Participant> participants) { this.participants = participants; }
	
	protected List<RelatedContract> relatedContracts;
	public List<RelatedContract> getRelatedContracts() { return relatedContracts; }
	public void setRelatedContracts(List<RelatedContract> relatedContracts) { this.relatedContracts = relatedContracts; }

	protected Type type;
	public Type getType() { return type; }
	public void setType(Type type) { this.type = type; }

	protected PaymentMethod paymentMethod;
	public PaymentMethod getPaymentMethod() { return paymentMethod; }
	public void setPaymentMethod(PaymentMethod paymentMethod) { this.paymentMethod = paymentMethod; }
	
	protected Limits limits;
	public Limits getLimits() { return limits; }
	public void setLimits(Limits limits) { this.limits = limits; }
	
	protected PhysicalMedium physicalMedium;
	public PhysicalMedium getPhysicalMedium() { return physicalMedium; }
	public void setPhysicalMedium(PhysicalMedium physicalMedium) { this.physicalMedium = physicalMedium; }
	
	protected Amount disposedBalance;
	public Amount getDisposedBalance() { return disposedBalance; }
	public void setDisposedBalance(Amount disposedBalance) { this.disposedBalance = disposedBalance; }
	
	protected Amount availableBalance;
	public Amount getAvailableBalance() { return availableBalance; }
	public void setAvailableBalance(Amount availableBalance) { this.availableBalance = availableBalance; }
	
	protected String endDate;
	public String getEndDate() { return endDate; }
	public void setEndDate(String endDate) { this.endDate = endDate; }
	
	protected String paymentDate;
	public String getPaymentDate() { return paymentDate; }
	public void setPaymentDate(String paymentDate) { this.paymentDate = paymentDate; }
	
	protected Amount outstandingDebtLocalCurrency;
	public Amount getOutstandingDebtLocalCurrency() { return outstandingDebtLocalCurrency; }
	public void setOutstandingDebtLocalCurrency(Amount outstandingDebtLocalCurrency) { this.outstandingDebtLocalCurrency = outstandingDebtLocalCurrency; }
	
	protected Amount outstandingDebt;
	public Amount getOutstandingDebt() { return outstandingDebt; }
	public void setOutstandingDebt(Amount outstandingDebt) { this.outstandingDebt = outstandingDebt; }
	
	protected PaymentData paymentData;
	public PaymentData getPaymentData() { return paymentData; }
	public void setPaymentData(PaymentData paymentData) { this.paymentData = paymentData; }
	
	protected Type brandAssociation;
	public Type getBrandAssociation() { return brandAssociation; }
	public void setBrandAssociation(Type brandAssociation) { this.brandAssociation = brandAssociation; }
	
	protected Indicators indicators;
	public Indicators getIndicators() { return indicators; }
	public void setIndicators(Indicators indicators) { this.indicators = indicators; }
	
	protected Product product;
	public Product getProduct() { return product; }
	public void setProduct(Product product) { this.product = product; }
	
	protected List<OperationalRestriction> operationalRestrictions;
	public List<OperationalRestriction> getOperationalRestrictions() {return operationalRestrictions;	}
	public void setOperationalRestrictions(List<OperationalRestriction> operationalRestrictions) {this.operationalRestrictions = operationalRestrictions;}

	protected Amount disposedBalanceLocalCurrency;
	public Amount getDisposedBalanceLocalCurrency() { return disposedBalanceLocalCurrency; }
	public void setDisposedBalanceLocalCurrency(Amount disposedBalanceLocalCurrency) { this.disposedBalanceLocalCurrency = disposedBalanceLocalCurrency; }
	
	protected Type branch;
	public Type getBranch() {	return branch; }
	public void setBranch(Type branch) { this.branch = branch; }
	
	//Variables de ListCards
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String number;
	public String getNumber() {		return number;	}
	public void setNumber(String number) {		this.number = number;	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String cardId;
	public String getCardId() {return cardId;}
	public void setCardId(String cardId) {this.cardId = cardId;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String holderName;
	public String getHolderName() {return holderName;}
	public void setHolderName(String holderName) {this.holderName = holderName;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Type numberType;
	public Type getNumberType() {return numberType;}
	public void setNumberType(Type numberType) {this.numberType = numberType;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Currency> currencies;
	public List<Currency> getCurrencies() {return currencies;}
	public void setCurrencies(List<Currency> currencies) {this.currencies = currencies;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Type cardType;
	public Type getCardType() {return cardType;}
	public void setCardType(Type cardType) {this.cardType = cardType;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Type physicalSupport;
	public Type getPhysicalSupport() {return physicalSupport;}
	public void setPhysicalSupport(Type physicalSupport) {this.physicalSupport = physicalSupport;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Type status;
	public Type getStatus() {return status;}
	public void setStatus(Type status) {this.status = status;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Type title;
	public Type getTitle() {return title;}
	public void setTitle(Type title) {this.title = title;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Image> images;
	public List<Image> getImages() {return images;}
	public void setImages(List<Image> images) {this.images = images;}
	
}
