
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Manager extends Person {

	protected String globalSegment;
	protected List<?> rest;

    public List<?> getRest() {
        return this.rest;
    }

	public void setRest(List<?> rest) {
		this.rest = rest;
	}

	public String getGlobalSegment() {
		return globalSegment;
	}

	public void setGlobalSegment(String globalSegment) {
		this.globalSegment = globalSegment;
	}
}