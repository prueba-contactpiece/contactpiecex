package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRentability {
	
	protected Date date;
	protected Money regularMargin;
	protected Money financialMargin;
	protected Fees fees;
	protected Money investmentAverageBalance;
	protected Money resourcesAverageBalance;
	protected Money disintermediationAverageBalance;
	protected Money totalServicesAmount;
	
	public Money getRegularMargin() {
		return regularMargin;
	}
	public void setRegularMargin(Money regularMargin) {
		this.regularMargin = regularMargin;
	}
	
	public Money getFinancialMargin() {
		return financialMargin;
	}
	public void setFinancialMargin(Money financialMargin) {
		this.financialMargin = financialMargin;
	}
	
	public Fees getFees() {
		return fees;
	}
	public void setFees(Fees fees) {
		this.fees = fees;
	}
	
	public Money getInvestmentAverageBalance() {
		return investmentAverageBalance;
	}
	public void setInvestmentAverageBalance(Money investmentAverageBalance) {
		this.investmentAverageBalance = investmentAverageBalance;
	}
	
	public Money getResourcesAverageBalance() {
		return resourcesAverageBalance;
	}
	public void setResourcesAverageBalance(Money resourcesAverageBalance) {
		this.resourcesAverageBalance = resourcesAverageBalance;
	}
	
	public Money getDisintermediationAverageBalance() {
		return disintermediationAverageBalance;
	}
	public void setDisintermediationAverageBalance(
			Money disintermediationAverageBalance) {
		this.disintermediationAverageBalance = disintermediationAverageBalance;
	}
	
	public Money getTotalServicesAmount() {
		return totalServicesAmount;
	}
	public void setTotalServicesAmount(Money totalServicesAmount) {
		this.totalServicesAmount = totalServicesAmount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
