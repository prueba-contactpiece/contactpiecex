package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import pe.com.bbva.fcc.dominio.serv.Pagination;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CardsDetail {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Cards> items;
	public List<Cards> getItems() { return items; }
	public void setItems(List<Cards> items) { this.items = items; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Pagination pagination;
	public Pagination getPagination() { return pagination; }
	public void setPagination(Pagination pagination) { this.pagination = pagination; }
	
}