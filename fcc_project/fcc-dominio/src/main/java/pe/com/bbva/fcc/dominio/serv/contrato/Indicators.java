package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Indicators {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  hasPaymentAssets;
	public String getHasPaymentAssets() { return hasPaymentAssets; }
	public void setHasPaymentAssets(String hasPaymentAssets) { this.hasPaymentAssets = hasPaymentAssets; }
	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  hasSafetyBox;
	public String getHasSafetyBox() { return hasSafetyBox; }
	public void setHasSafetyBox(String hasSafetyBox) { this.hasSafetyBox = hasSafetyBox; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  id;
	public String getId() {	return id;}
	public void setId(String id) {	this.id = id;}
	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
 	protected String  isActive;
	public String getIsActive() {return isActive;}
	public void setIsActive(String isActive) {	this.isActive = isActive;} 
    
}
