package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonInclude;

public class HoldBacks {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money pending;
	public Money getPending() { return pending; }
	public void setPending(Money pending) { this.pending = pending; }
}
