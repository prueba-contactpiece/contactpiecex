package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PhysicalMedium {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected PlasticLifeCycle plasticLifeCycle;
	public PlasticLifeCycle getPlasticLifeCycle() { return plasticLifeCycle; }
	public void setPlasticLifeCycle(PlasticLifeCycle plasticLifeCycle) { this.plasticLifeCycle = plasticLifeCycle; }
	
}
