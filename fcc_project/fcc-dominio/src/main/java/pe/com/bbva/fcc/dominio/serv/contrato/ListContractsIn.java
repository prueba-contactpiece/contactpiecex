package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonInclude;
public class ListContractsIn {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String customerId;
	public String getCustomerId() { return customerId;	}
	public void setCustomerId(String customerId) { this.customerId = customerId; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)  
	protected IdentityDocument   identityDocument;
	public    IdentityDocument getIdentityDocument() { return identityDocument; }
	public void setIdentityDocument(IdentityDocument identityDocument) { this.identityDocument = identityDocument; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Contract  contract;
	public Contract getContract() { return contract; }
	public void setContract(Contract contract) { this.contract = contract; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String hasPack;
	public String getHasPack() { return hasPack; }
	public void setHasPack(String hasPack) { this.hasPack = hasPack;	}

}
