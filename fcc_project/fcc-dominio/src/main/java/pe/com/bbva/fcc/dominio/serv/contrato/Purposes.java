package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Purposes {

	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
//	private List<OptionsList> detailedPurposes;
//	public List<OptionsList> getDetailedPurposes() { return detailedPurposes; }
//	public void setDetailedPurposes(List<OptionsList> detailedPurposes) { this.detailedPurposes = detailedPurposes; }
}
