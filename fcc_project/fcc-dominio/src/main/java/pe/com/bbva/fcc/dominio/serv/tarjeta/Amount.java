package pe.com.bbva.fcc.dominio.serv.tarjeta;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class Amount {
	
	protected BigDecimal amount;
	public BigDecimal getAmount() { return amount; }
	public void setAmount(BigDecimal amount) { this.amount = amount; }
	
	protected String currency;
	public String getCurrency() { return currency; }
	public void setCurrency(String currency) { this.currency = currency; }
	
}
