package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AmortizationSchedule {
	
	protected List<DetailedAmortizationPayments> detailedAmortizationPayments;
	public List<DetailedAmortizationPayments> getDetailedAmortizationPayments() { return detailedAmortizationPayments; }
	public void setDetailedAmortizationPayments(List<DetailedAmortizationPayments> detailedAmortizationPayments) { this.detailedAmortizationPayments = detailedAmortizationPayments; }
	
	protected Money capitalTotalAmount;
	public Money getCapitalTotalAmount() { return capitalTotalAmount; }
	public void setCapitalTotalAmount(Money capitalTotalAmount) { this.capitalTotalAmount = capitalTotalAmount; }
	
	protected Money interestTotalAmount;
	public Money getInterestTotalAmount() {	return interestTotalAmount; }
	public void setInterestTotalAmount(Money interestTotalAmount) { this.interestTotalAmount = interestTotalAmount; }
	
	protected Money totalAmount;	
	public Money getTotalAmount() {	return totalAmount; }
	public void setTotalAmount(Money totalAmount) { this.totalAmount = totalAmount;	}
	
	protected Money subsidizedTotalAmount;
	public Money getSubsidizedTotalAmount() { return subsidizedTotalAmount; }
	public void setSubsidizedTotalAmount(Money subsidizedTotalAmount) { this.subsidizedTotalAmount = subsidizedTotalAmount; }
}