package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class PaymentMethod {

	protected Type type;
	public Type getType() {	return type; }
	public void setType(Type type) { this.type = type; }
	
	protected Amount minimunAmountLocalCurrency;
	public Amount getMinimunAmountLocalCurrency() {	return minimunAmountLocalCurrency; }
	public void setMinimunAmountLocalCurrency(Amount minimunAmountLocalCurrency) { this.minimunAmountLocalCurrency = minimunAmountLocalCurrency; }
	
	protected Amount minimunAmount;
	public Amount getMinimunAmount() { return minimunAmount;	}
	public void setMinimunAmount(Amount minimunAmount) { this.minimunAmount = minimunAmount;	}
	
	protected Amount amountLocalCurrency;
	public Amount getAmountLocalCurrency() { return amountLocalCurrency; }
	public void setAmountLocalCurrency(Amount amountLocalCurrency) { this.amountLocalCurrency = amountLocalCurrency; }
	
	protected Amount amount;
	public Amount getAmount() { return amount; }
	public void setAmount(Amount amount) { this.amount = amount; }

}
