package pe.com.bbva.fcc.dominio.serv.gt;

public class GrantingTicketResponse {
	
	private AuthenticationResponse authenticationResponse;
	public AuthenticationResponse getAuthenticationResponse() { return authenticationResponse; }
	public void setAuthenticationResponse(AuthenticationResponse authenticationResponse) { this.authenticationResponse = authenticationResponse; }

}
