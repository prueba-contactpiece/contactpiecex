package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Type {
	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	protected OptionsList type;
	public OptionsList getType() { return type; }
	public void setType(OptionsList type) { this.type = type; }
}
