package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Relationship {
	
	protected OptionsList type;
	public OptionsList getType() { return type; }
	public void setType(OptionsList type) { this.type = type; }
}