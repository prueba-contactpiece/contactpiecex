package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Taxes {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Amount totalamount;
	public Amount getTotalAmount() { return totalamount; }
	public void setTotalAmount(Amount totalamount) { this.totalamount = totalamount; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected DetailTaxes detailtaxes;
	public DetailTaxes getDetailTaxes() { return detailtaxes; }
	public void setDetailTaxes(DetailTaxes detailtaxes) { this.detailtaxes = detailtaxes; }

}