
package pe.com.bbva.fcc.dominio.serv.cliente;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class EconomicExtendedData {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String creditBureauClassification;
	public String getCreditBureauClassification() { return creditBureauClassification; }
    public void setCreditBureauClassification(String value) { this.creditBureauClassification = value; }
        
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String economicGroup;
	public String getEconomicGroup() { return economicGroup; }
    public void setEconomicGroup(String value) { this.economicGroup = value; }
}