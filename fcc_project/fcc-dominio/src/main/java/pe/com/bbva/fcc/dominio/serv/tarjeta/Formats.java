package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class Formats {
	
	protected String pan;
	public String getPan() { return pan; }
	public void setPan(String pan) { this.pan = pan; }

}
