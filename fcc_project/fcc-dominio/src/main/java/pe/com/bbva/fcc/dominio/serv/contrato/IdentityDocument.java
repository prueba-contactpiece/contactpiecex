
package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class IdentityDocument {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList type;
	public void setType(OptionsList type) { this.type = type; }
    public OptionsList getType() { return this.type; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String number;
	public String getNumber() { return number; }
    public void setNumber(String value) { this.number = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Date expirationDate;
    public Date getExpirationDate() { return expirationDate; }
    public void setExpirationDate(Date value) { this.expirationDate = value; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Type documentType;
    public Type getDocumentType() { return documentType; }
	public void setDocumentType(Type documentType) { this.documentType = documentType; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private String documentNumber;
	public String getDocumentNumber() { return documentNumber; }
	public void setDocumentNumber(String documentNumber) { this.documentNumber = documentNumber; }
}