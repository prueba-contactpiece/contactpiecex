package pe.com.bbva.fcc.dominio.serv.tarjeta;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown=true)

public class RelatedContracts {

	protected List<RelatedContract> relatedContracts;
	public List<RelatedContract> getRelatedContracts() { return relatedContracts; }
	public void setRelatedContracts(List<RelatedContract> relatedContracts) { this.relatedContracts = relatedContracts; }
	
}
