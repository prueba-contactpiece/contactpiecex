package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Blocking {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Status status;
	public Status getStatus() { return status; }
	public void setStatus(Status status) { this.status = status; }
	
}
