
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Person {
	
	/**Version 2.0*/
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String id;
	public String getId() { return id; }
    public void setId(String value) { this.id = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<IdentityDocument> identityDocument;
	public List<IdentityDocument> getIdentityDocument() { return identityDocument; }
    public void setIdentityDocument(List<IdentityDocument> value) { this.identityDocument = value; }
        
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String name;
	public String getName() { return name; }
    public void setName(String value) { this.name = value; }
        
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String lastName;
	public String getLastName() { return lastName; }
    public void setLastName(String value) { this.lastName = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String mothersLastName;
	public String getMothersLastName() { return mothersLastName; }
    public void setMothersLastName(String value) { this.mothersLastName = value; }    
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date birthDate;
	public Date getBirthDate() { return birthDate; }
    public void setBirthDate(Date value) { this.birthDate = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Boolean isCustomer;
	public void setIsCustomer(Boolean value) { this.isCustomer = value; }    
	public Boolean getIsCustomer() { return isCustomer; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected List<Address> addresses;
	public List<Address> getAddresses() { return this.addresses; }
	public void setAddresses(List<Address> addresses) { this.addresses = addresses; }	
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected EconomicData economicData;
	public EconomicData getEconomicData() { return economicData; }
    public void setEconomicData(EconomicData value) { this.economicData = value; }    
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected CompanyInformation companyInformation;
	public CompanyInformation getCompanyInformation() { return companyInformation; }
	public void setCompanyInformation(CompanyInformation companyInformation) { this.companyInformation = companyInformation; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected ManagementData managementData;
	public ManagementData getManagementData() { return managementData; }
	public void setManagementData(ManagementData managementData) { this.managementData = managementData; }
        
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected PersonExtendedData extendedData;
	public PersonExtendedData getExtendedData() { return extendedData; }
    public void setExtendedData(PersonExtendedData value) { this.extendedData = value; }    
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected DelinquencyRiskData delinquencyRiskData;
	public DelinquencyRiskData getDelinquencyRiskData() { return delinquencyRiskData; }
	public void setDelinquencyRiskData(DelinquencyRiskData delinquencyRiskData) { this.delinquencyRiskData = delinquencyRiskData; }	
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected ContactInformation contactInformation;
	public ContactInformation getContactInformation() { return contactInformation; }
	public void setContactInformation(ContactInformation contactInformation) { this.contactInformation = contactInformation; }	
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected PhoneFormat phoneFormat;
	public PhoneFormat getPhoneFormat() { return phoneFormat; }
	public void setPhoneFormat(PhoneFormat phoneFormat) { this.phoneFormat = phoneFormat; }	
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected EconomicExtendedData economicExtendedData;
	public EconomicExtendedData getEconomicExtendedData() { return economicExtendedData; }
    public void setEconomicExtendedData(EconomicExtendedData value) { this.economicExtendedData = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Person> legalRepresentatives;
	public List<Person> getLegalRepresentatives() { return this.legalRepresentatives; }
	public void setLegalRepresentatives(List<Person> legalRepresentatives) { this.legalRepresentatives = legalRepresentatives; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<ContactInformation> contactsInformation;
	public List<ContactInformation> getContactsInformation() { return contactsInformation; }
	public void setContactsInformation(List<ContactInformation> contactsInformation) { this.contactsInformation = contactsInformation; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Classification classification;
	public Classification getClassification() { return classification; }
    public void setClassification(Classification value) { this.classification = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected OptionsList type;
	public OptionsList getType() { return type; }
    public void setType(OptionsList value) { this.type = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected DelinquencyRiskData prospectDelinquencyRiskData;
	public DelinquencyRiskData getProspectDelinquencyRiskData() { return prospectDelinquencyRiskData; }
    public void setProspectDelinquencyRiskData(DelinquencyRiskData prospectDelinquencyRiskData) { this.prospectDelinquencyRiskData = prospectDelinquencyRiskData; }
		
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Relationship relationship;
	public Relationship getRelationship() { return relationship; }
	public void setRelationship(Relationship relationship) { this.relationship = relationship; }	
	
	/** Getters and Setters corresponden a la version 1.0 */
	/**Version 1.0*/  

//  protected Date stablismentDate;
//  protected List<OptionsList> sources;
//  protected String referene;
//	protected Address legalAddress;

    /**
     * Obtiene el valor de la propiedad stablismentDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
//    public Date getStablismentDate() {
//        return stablismentDate;
//    }

    /**
     * Define el valor de la propiedad stablismentDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
//    public void setStablismentDate(Date value) {
//        this.stablismentDate = value;
//    }

    /**
     * Obtiene el valor de la propiedad legalAddress.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
//    public Address getLegalAddress() {
//        return legalAddress;
//    }

    /**
     * Define el valor de la propiedad legalAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
//    public void setLegalAddress(Address value) {
//        this.legalAddress = value;
//    }
    /**
     * Obtiene el valor de la propiedad referene.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
//    public String getReferene() {
//        return referene;
//    }

    /**
     * Define el valor de la propiedad referene.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
//    public void setReferene(String value) {
//        this.referene = value;
//    }
	
//	public void setSources(List<OptionsList> sources) {
//		this.sources = sources;
//	}

}
