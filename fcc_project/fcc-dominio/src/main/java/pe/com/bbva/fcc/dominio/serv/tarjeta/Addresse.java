package pe.com.bbva.fcc.dominio.serv.tarjeta;

public class Addresse {
	protected String isMainAddress;
	public String getIsMainAddress() { return isMainAddress; }
	public void setIsMainAddress(String isMainAddress) { this.isMainAddress = isMainAddress; }
	
	private String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	private Type streetType;
	public Type getStreetType() { return streetType; }
	public void setStreetType(Type streetType) { this.streetType = streetType; }
	
	private String state;
	public String getState() { return state; }
	public void setState(String state) { this.state = state; }
	
	private String ubigeo;
	public String getUbigeo() { return ubigeo; }
	public void setUbigeo(String ubigeo) { this.ubigeo = ubigeo; }
}
