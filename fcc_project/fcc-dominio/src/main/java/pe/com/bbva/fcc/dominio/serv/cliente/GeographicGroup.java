package pe.com.bbva.fcc.dominio.serv.cliente;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class GeographicGroup {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Type type;
	public Type getType() { return type; }
	public void setType(Type type) { this.type = type; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String code;
	public String getCode() { return code; }
    public void setCode(String value) { this.code = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String name;
	public String getName() { return name; }
    public void setName(String value) { this.name = value; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected OptionsList geographicGroupType;
	public OptionsList getGeographicGroupType() {return geographicGroupType;}
	public void setGeographicGroupType(OptionsList geographicGroupType) {this.geographicGroupType = geographicGroupType;}
	
}