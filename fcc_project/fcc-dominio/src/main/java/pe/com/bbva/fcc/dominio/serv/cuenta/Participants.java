package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Participants {
	protected String name;
	protected String lastname;
	protected String motherslastname;

	/**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }
    
    
    /**
     * Obtiene el valor de la propiedad lastname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastname;
    }

    /**
     * Define el valor de la propiedad lastname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastname = value;
    }
	
    
    /**
     * Obtiene el valor de la propiedad motherslastname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMothersLastName() {
        return motherslastname;
    }

    /**
     * Define el valor de la propiedad motherslastname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMothersLastName(String value) {
        this.motherslastname = value;
    }
    
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected RelationShip relationship;
	public RelationShip getRelationShip() { return relationship; }
	public void setRelationShip(RelationShip relationship) { this.relationship = relationship; }

    
}
