package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

public class NextFractionInformation {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date  date;
	public Date getDate() { return date; }
	public void setDate(Date date) { this.date = date; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String id;
	public String getId() { return id; }
    public void setId(String id) { this.id = id; }
}
