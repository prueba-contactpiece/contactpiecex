package pe.com.bbva.fcc.dominio.serv.tarjeta;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

public class CardBlock {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String blockId;

	public String getBlockId() {
		return blockId;
	}

	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date blockDate;

	public Date getBlockDate() {
		return blockDate;
	}

	public void setBlockDate(Date blockDate) {
		this.blockDate = blockDate;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Reason reason;

	public Reason getReason() {
		return reason;
	}

	public void setReason(Reason reason) {
		this.reason = reason;
	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date reference;

	public Date getReference() {
		return reference;
	}

	public void setReference(Date reference) {
		this.reference = reference;
	}
}
