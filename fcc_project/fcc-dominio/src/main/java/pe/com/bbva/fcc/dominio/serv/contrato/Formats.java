package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Formats {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String pan;
	public String getPan() { return pan; }
	public void setPan(String pan) { this.pan = pan; }
	
}
