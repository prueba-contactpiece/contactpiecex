package pe.com.bbva.fcc.dominio.serv.tarjeta;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown=true)

public class Participant {

	protected String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	protected Relationship relationship;
	public Relationship getRelationship() {return relationship;}
	public void setRelationship(Relationship relationship) {this.relationship = relationship;}
	
	protected List<Addresse> Addresses;
	public List<Addresse> getAddresses() { return Addresses; }
	public void setAddresses(List<Addresse> addresses) { Addresses = addresses; }
}