
package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;
import pe.com.bbva.fcc.dominio.serv.Pagination;

public class ListContractsOut {
	protected List<ContractAction> items;
	public List<ContractAction> getItems() { return items; }
	public void setItems(List<ContractAction> items) { this.items = items; }
	
	protected Pagination pagination;	
	public Pagination getPagination() { return pagination; }
	public void setPagination(Pagination pagination) { this.pagination = pagination; }	
}
