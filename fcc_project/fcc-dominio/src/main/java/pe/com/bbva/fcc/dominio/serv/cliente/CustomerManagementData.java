package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CustomerManagementData {
	
	protected List<Operative> operatives;

	/**
     * Obtiene el valor de la propiedad operatives. 
     * @return
     *     possible object is
     *     {@link List<Operative> }
     */
	public List<Operative> getOperatives() {
		return operatives;
	}
	/**
     * Define el valor de la propiedad operatives.
     * @param value
     *     allowed object is
     *     {@link List<Operative> }
     */
	public void setOperatives(List<Operative> operatives) {
		this.operatives = operatives;
	}
}
