package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonInclude;

public class AcquisitionInformation {
	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  firstPaymentDate;
	public String getFirstPaymentDate() { return firstPaymentDate; }
	public void setFirstPaymentDate(String firstPaymentDate) { this.firstPaymentDate = firstPaymentDate; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money acquisitionAmount;
	public Money getAcquisitionAmount() { return acquisitionAmount; }
	public void setAcquisitionAmount(Money acquisitionAmount) { this.acquisitionAmount = acquisitionAmount; }
	
}
