package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class EconomicData {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected OptionsList sectorType;
	public OptionsList getSectorType() { return sectorType; }
	public void setSectorType(OptionsList sectorType) { this.sectorType = sectorType; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList subSectorType;
	public OptionsList getSubSectorType() { return subSectorType; }
    public void setSubSectorType(OptionsList value) { this.subSectorType = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList occupationType;
	public OptionsList getOccupationType() { return occupationType; }
    public void setOccupationType(OptionsList value) { this.occupationType = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList job;
	public OptionsList getJob() { return job; }
    public void setJob(OptionsList value) { this.job = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList profession;
	public OptionsList getProfession() { return profession; }
    public void setProfession(OptionsList value) { this.profession = value; }    
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList economicActivity;
	public OptionsList getEconomicActivity() { return economicActivity; }
    public void setEconomicActivity(OptionsList value) { this.economicActivity = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList secondaryEconomicActivity;
	public OptionsList getSecondaryEconomicActivity() { return secondaryEconomicActivity; }
    public void setSecondaryEconomicActivity(OptionsList value) { this.secondaryEconomicActivity = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList tertiaryEconomicActivity;
	public OptionsList getTertiaryEconomicActivity() { return tertiaryEconomicActivity; }
    public void setTertiaryEconomicActivity(OptionsList value) { this.tertiaryEconomicActivity = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Date economicActivityQuestionarie;
	public Date getEconomicActivityQuestionarie() { return economicActivityQuestionarie; }
    public void setEconomicActivityQuestionarie(Date value) { this.economicActivityQuestionarie = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money fixedIncomes;
	public Money getFixedIncomes() { return fixedIncomes; }
    public void setFixedIncomes(Money value) { this.fixedIncomes = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money variableIncomes;
	public Money getVariableIncomes() { return variableIncomes; }
    public void setVariableIncomes(Money value) { this.variableIncomes = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money otherPayments;
	public Money getOtherPayments() { return otherPayments; }
    public void setOtherPayments(Money value) { this.otherPayments = value; }    
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money housingIncomes;
	public Money getHousingIncomes() { return housingIncomes; }
    public void setHousingIncomes(Money value) { this.housingIncomes = value; }    
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money declarativeIncome;
	public Money getDeclarativeIncome() { return declarativeIncome; }
    public void setDeclarativeIncome(Money value) { this.declarativeIncome = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money totalAnnualTurnover;
	public Money getTotalAnnualTurnover() { return totalAnnualTurnover; }
    public void setTotalAnnualTurnover(Money value) { this.totalAnnualTurnover = value; }    
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money monthlyMortgageOutcome;
	public Money getMonthlyMortgageOutcome() { return monthlyMortgageOutcome; }
    public void setMonthlyMortgageOutcome(Money value) { this.monthlyMortgageOutcome = value; }    
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected EconomicExtendedData extendedData;
	public EconomicExtendedData getExtendedData() { return extendedData; }
    public void setExtendedData(EconomicExtendedData value) { this.extendedData = value; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList workplace;
	public OptionsList getWorkplace() { return workplace; }
	public void setWorkplace(OptionsList workplace) { this.workplace = workplace; }
    
    
}