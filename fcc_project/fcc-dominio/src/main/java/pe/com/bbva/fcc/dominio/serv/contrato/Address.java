
package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Address {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String location;
	public String getLocation() { return location; }
    public void setLocation(String value) { this.location = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String id;
	public String getId() { return id; }
    public void setId(String value) { this.id = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Reference owner;
	public Reference getOwner() { return owner; }
    public void setOwner(Reference value) { this.owner = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList type;
	public OptionsList getType() { return type; }
    public void setType(OptionsList value) { this.type = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList streetType;
	public OptionsList getStreetType() { return streetType; }
    public void setStreetType(OptionsList value) { this.streetType = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String streetName;
	public String getStreetName() { return streetName; }
    public void setStreetName(String value) { this.streetName = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String streetNumber;
	public String getStreetNumber() { return streetNumber; }
    public void setStreetNumber(String value) { this.streetNumber = value; }    
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String city;
	public String getCity() { return city; }
    public void setCity(String value) { this.city = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String zipCode;
	public String getZipCode() { return zipCode; }
    public void setZipCode(String value) { this.zipCode = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String block;
	public String getBlock() { return block; }
    public void setBlock(String value) { this.block = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String stair;
	public String getStair() { return stair; }
    public void setStair(String value) { this.stair = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String floor;
	public String getFloor() { return floor; }
    public void setFloor(String value) { this.floor = value; }    
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String door;
	public String getDoor() { return door; }
    public void setDoor(String value) { this.door = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String name;
	public String getName() { return name; }
    public void setName(String value) { this.name = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Date startingResidenceDate;
	public Date getStartingResidenceDate() { return startingResidenceDate; }
    public void setStartingResidenceDate(Date value) { this.startingResidenceDate = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String alias;
	public String getAlias() { return alias; }
    public void setAlias(String value) { this.alias = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String additionalInformation;
	public String getAdditionalInformation() { return additionalInformation; }
    public void setAdditionalInformation(String value) { this.additionalInformation = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected List<GeographicGroup> geographicGroup;
	public List<GeographicGroup> getGeographicGroup() { return this.geographicGroup; }    
    public void setGeographicGroup(List<GeographicGroup> geographicGroup){ this.geographicGroup = geographicGroup; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Boolean hasContractsAssociated;
	public Boolean isHasContractsAssociated() { return hasContractsAssociated; }
    public void setHasContractsAssociated(Boolean value) { this.hasContractsAssociated = value; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList ownershipType;
	public OptionsList getOwnershipType() { return ownershipType; }
    public void setOwnershipType(OptionsList value) { this.ownershipType = value; } 
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String interiorNumber;
	public String getInteriorNumber() { return interiorNumber; }
    public void setInteriorNumber(String value) { this.interiorNumber = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String housingDevelopmentName;
	public String getHousingDevelopmentName() { return housingDevelopmentName; }
    public void setHousingDevelopmentName(String value) { this.housingDevelopmentName = value; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList domicileType;
	public OptionsList getDomicileType() { return domicileType; }
    public void setDomicileType(OptionsList value) { this.domicileType = value;}
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList country;
	public OptionsList getCountry() { return country; }
	public void setCountry(OptionsList country) { this.country = country; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private List<GeographicGroup> geographicGroups;
	public List<GeographicGroup> getGeographicGroups() { return geographicGroups; }
	public void setGeographicGroups(List<GeographicGroup> geographicGroups) { this.geographicGroups = geographicGroups; }

}
