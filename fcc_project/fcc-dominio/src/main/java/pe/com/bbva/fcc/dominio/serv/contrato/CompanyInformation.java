package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown=true)
public class CompanyInformation {

	protected OptionsList source;
	protected Integer numberOfEmployees;
	protected Integer numberOfShareholders;
	
	/**
	* 
	* @return
	* The numberOfEmployees
	*/
	public Integer getNumberOfEmployees() {
		return numberOfEmployees;
	}
	
	/**
	* 
	* @param numberOfEmployees
	* The numberOfEmployees
	*/
	public void setNumberOfEmployees(Integer numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}
	
	/**
	* 
	* @return
	* The numberOfShareholders
	*/
	public Integer getNumberOfShareholders() {
		return numberOfShareholders;
	}
	
	/**
	* 
	* @param numberOfShareholders
	* The numberOfShareholders
	*/
	public void setNumberOfShareholders(Integer numberOfShareholders) {
		this.numberOfShareholders = numberOfShareholders;
	}

	public OptionsList getSource() {
		return source;
	}

	public void setSource(OptionsList source) {
		this.source = source;
	}

}
