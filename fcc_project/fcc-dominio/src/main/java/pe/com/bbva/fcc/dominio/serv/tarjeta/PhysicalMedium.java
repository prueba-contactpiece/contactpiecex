package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PhysicalMedium {

	protected PlasticLifeCycle plasticLifeCycle;
	public PlasticLifeCycle getPlasticLifeCycle() { return plasticLifeCycle; }
	public void setPlasticLifeCycle(PlasticLifeCycle plasticLifeCycle) { this.plasticLifeCycle = plasticLifeCycle; }
	
}
