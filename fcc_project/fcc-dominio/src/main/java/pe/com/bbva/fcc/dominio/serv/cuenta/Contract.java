package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Contract {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Amount currentBalance;
	public Amount getCurrentBalance() { return currentBalance; }
	public void setScheme(Amount currentBalance) { this.currentBalance = currentBalance; }
	
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Amount availableBalance;
	public Amount getAvailableBalance() { return availableBalance; }
	public void setAvailableBalance(Amount availableBalance) { this.availableBalance = availableBalance; }
}
