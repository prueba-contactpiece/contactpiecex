
package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PhoneFormat {

    protected String internationalPhoneNetworkSelector;
    protected String countryPhoneCode;
    protected String regionalPhoneCode;
    protected String subscriberPhoneNumber;
    protected String extensionPhoneNumber;

    /**
     * Obtiene el valor de la propiedad internationalPhoneNetworkSelector.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternationalPhoneNetworkSelector() {
        return internationalPhoneNetworkSelector;
    }

    /**
     * Define el valor de la propiedad internationalPhoneNetworkSelector.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternationalPhoneNetworkSelector(String value) {
        this.internationalPhoneNetworkSelector = value;
    }

    /**
     * Obtiene el valor de la propiedad countryPhoneCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryPhoneCode() {
        return countryPhoneCode;
    }

    /**
     * Define el valor de la propiedad countryPhoneCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryPhoneCode(String value) {
        this.countryPhoneCode = value;
    }

    /**
     * Obtiene el valor de la propiedad regionalPhoneCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegionalPhoneCode() {
        return regionalPhoneCode;
    }

    /**
     * Define el valor de la propiedad regionalPhoneCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegionalPhoneCode(String value) {
        this.regionalPhoneCode = value;
    }

    /**
     * Obtiene el valor de la propiedad subscriberPhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubscriberPhoneNumber() {
        return subscriberPhoneNumber;
    }

    /**
     * Define el valor de la propiedad subscriberPhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubscriberPhoneNumber(String value) {
        this.subscriberPhoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad extensionPhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtensionPhoneNumber() {
        return extensionPhoneNumber;
    }

    /**
     * Define el valor de la propiedad extensionPhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtensionPhoneNumber(String value) {
        this.extensionPhoneNumber = value;
    }

}
