package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Reason {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
