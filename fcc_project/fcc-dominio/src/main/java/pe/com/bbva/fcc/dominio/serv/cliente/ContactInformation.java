
package pe.com.bbva.fcc.dominio.serv.cliente;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ContactInformation {

    protected String name;
    protected OptionsList type;
    protected Boolean preferential;
    protected Boolean checked;
    protected Boolean contactSecurityOperation;
    protected Integer fromHour;
    protected Integer toHour;
    protected String additionalInformation;
    protected Boolean allowAd;
    protected OptionsList contactMode;
    protected String phoneNumber;
    protected String email;
    protected Boolean insistNotAllowed;
//    protected PhoneFormat format;
    protected PhoneFormat formats;

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setType(OptionsList value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad preferential.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreferential() {
        return preferential;
    }

    /**
     * Define el valor de la propiedad preferential.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreferential(Boolean value) {
        this.preferential = value;
    }

    /**
     * Obtiene el valor de la propiedad checked.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChecked() {
        return checked;
    }

    /**
     * Define el valor de la propiedad checked.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChecked(Boolean value) {
        this.checked = value;
    }

    /**
     * Obtiene el valor de la propiedad contactSecurityOperation.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContactSecurityOperation() {
        return contactSecurityOperation;
    }

    /**
     * Define el valor de la propiedad contactSecurityOperation.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactSecurityOperation(Boolean value) {
        this.contactSecurityOperation = value;
    }

    /**
     * Obtiene el valor de la propiedad fromHour.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFromHour() {
        return fromHour;
    }

    /**
     * Define el valor de la propiedad fromHour.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFromHour(Integer value) {
        this.fromHour = value;
    }

    /**
     * Obtiene el valor de la propiedad toHour.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getToHour() {
        return toHour;
    }

    /**
     * Define el valor de la propiedad toHour.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setToHour(Integer value) {
        this.toHour = value;
    }

    /**
     * Obtiene el valor de la propiedad additionalInformation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalInformation() {
        return additionalInformation;
    }

    /**
     * Define el valor de la propiedad additionalInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalInformation(String value) {
        this.additionalInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad allowAd.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowAd() {
        return allowAd;
    }

    /**
     * Define el valor de la propiedad allowAd.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowAd(Boolean value) {
        this.allowAd = value;
    }

    /**
     * Obtiene el valor de la propiedad contactMode.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getContactMode() {
        return contactMode;
    }

    /**
     * Define el valor de la propiedad contactMode.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setContactMode(OptionsList value) {
        this.contactMode = value;
    }

    /**
     * Obtiene el valor de la propiedad phoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Define el valor de la propiedad phoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad insistNotAllowed.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isInsistNotAllowed() {
        return insistNotAllowed;
    }

    /**
     * Define el valor de la propiedad insistNotAllowed.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setInsistNotAllowed(Boolean value) {
        this.insistNotAllowed = value;
    }

    /**
     * Obtiene el valor de la propiedad format.
     * 
     * @return
     *     possible object is
     *     {@link PhoneFormat }
     *     
     */
//    public PhoneFormat getFormat() {
//        return format;
//    }

    /**
     * Define el valor de la propiedad format.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneFormat }
     *     
     */
//    public void setFormat(PhoneFormat value) {
//        this.format = value;
//    }
    /**
     * Obtiene el valor de la propiedad formats.
     * 
     * @return
     *     possible object is
     *     {@link PhoneFormat }
     */
	public PhoneFormat getFormats() {
		return formats;
	}

	/**
     * Define el valor de la propiedad formats.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneFormat }
     *     
     */
	public void setFormats(PhoneFormat formats) {
		this.formats = formats;
	}

}
