
package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ProductProposal {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String id;
    public String getId() { return id;  }
    public void setId(String value) { this.id = value; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String name;
    public String getName() { return name; }
    public void setName(String value) { this.name = value; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected List<OptionsList> commercialClassifications;
	public List<OptionsList> getCommercialClassifications() {return commercialClassifications;}
	public void setCommercialClassifications(List<OptionsList> commercialClassifications) {	this.commercialClassifications = commercialClassifications;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected List<Purposes> purposes;
	public List<Purposes> getPurposes() {return purposes;}
	public void setPurposes(List<Purposes> purposes) {this.purposes = purposes;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList productType;
	public OptionsList getProductType() {return productType;	}
	public void setProductType(OptionsList productType) {this.productType = productType;}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList title;
	public OptionsList getTitle() {return title;}
	public void setTitle(OptionsList title) {this.title = title;}
	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String bin;
	public String getBin() {return bin;	}
	public void setBin(String bin) {this.bin = bin;	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList productClassification;
	public OptionsList getProductClassification() {return productClassification;}
	public void setProductClassification(OptionsList productClassification) {this.productClassification = productClassification;}
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected ProductConfiguration productConfiguration;
	public ProductConfiguration getProductConfiguration() {return productConfiguration;}
	public void setProductConfiguration(ProductConfiguration productConfiguration) {this.productConfiguration = productConfiguration;}

}