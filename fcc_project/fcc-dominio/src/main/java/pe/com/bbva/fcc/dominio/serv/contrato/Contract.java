package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Contract {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected ProductProposal product;
	public ProductProposal getProduct() { return product; }
    public void setProduct(ProductProposal product) { this.product = product; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Branch branch;
	public Branch getBranch() { return branch; }
    public void setBranch(Branch branch) { this.branch = branch; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Currency currency;
	public Currency getCurrency() { return currency; }
    public void setCurrency(Currency currency) { this.currency = currency; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Date openingDate;
    public Date getOpeningDate() { return openingDate; }
    public void setOpeningDate(Date openingDate) { this.openingDate = openingDate; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Date expirationDate;
    public Date getExpirationDate() { return expirationDate; }
    public void setExpirationDate(Date expirationDate) { this.expirationDate = expirationDate; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected List<Participant> participants;
    public List<Participant> getParticipants() { return participants; }
    public void setParticipants(List<Participant> participants) { this.participants = participants; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Status status;
    public Status getStatus() { return status; }
    public void setStatus(Status status) { this.status = status; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<CardsDetail> cards;
	public List<CardsDetail> getCards() { return cards; }
	public void setCards(List<CardsDetail> cards) { this.cards = cards; }
    
   
}
