package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonIgnoreProperties(ignoreUnknown=true)
public class Cards {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String id;
	public String getId() {	return id; }
	public void setId(String id) { this.id = id; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected ProductProposal product;
	public ProductProposal getProduct() { return product; }
	public void setProduct(ProductProposal product) { this.product = product; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Formats formats;
	public Formats getFormats() { return formats; }
	public void setFormats(Formats formats) { this.formats = formats; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected PhysicalMedium physicalMedium;
	public PhysicalMedium getPhysicalMedium() { return physicalMedium; }
	public void setPhysicalMedium(PhysicalMedium physicalMedium) { this.physicalMedium = physicalMedium; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Blocking blocking;
	public Blocking getBlocking() { return blocking; }
	public void setBlocking(Blocking blocking) { this.blocking = blocking; }
	
}