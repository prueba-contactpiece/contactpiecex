package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Origin {
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String datosOrigin;
	public String getDatosOrigin() { return datosOrigin; }
	public void setDatosOrigin(String datosOrigin) { this.datosOrigin = datosOrigin; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Type originClassification;
	public Type getOriginClassification() { return originClassification; }
	public void setoriginClassification(Type originClassification) { this.originClassification = originClassification; }
}
