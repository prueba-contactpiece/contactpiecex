package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DetailedAmortizationPayments {

	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	protected Date dueDate;
	public Date getDueDate() { return dueDate; }
	public void setDueDate(Date dueDate) { this.dueDate = dueDate; }
	
	protected Money capitalAmount;
	public Money getCapitalAmount() { return capitalAmount; }
	public void setCapitalAmount(Money capitalAmount) { this.capitalAmount = capitalAmount;	}
	
	protected Money interestAmount;
	public Money getInterestAmount() { return interestAmount; }
	public void setInterestAmount(Money interestAmount) { this.interestAmount = interestAmount;	}
	
	protected Money pendingAmount;
	public Money getPendingAmount() { return pendingAmount; }
	public void setPendingAmount(Money pendingAmount) { this.pendingAmount = pendingAmount; }
	
	protected Money subsidizedAmount;
	public Money getSubsidizedAmount() { return subsidizedAmount; }
	public void setSubsidizedAmount(Money subsidizedAmount) { this.subsidizedAmount = subsidizedAmount; }
	
	protected Money amount;
	public Money getAmount() { return amount; }
	public void setAmount(Money amount) { this.amount = amount;	}
	
	protected ExpensesAmount expensesAmount;
	public ExpensesAmount getExpensesAmount() { return expensesAmount; }
	public void setExpensesAmount(ExpensesAmount expensesAmount) { this.expensesAmount = expensesAmount; }
	
}