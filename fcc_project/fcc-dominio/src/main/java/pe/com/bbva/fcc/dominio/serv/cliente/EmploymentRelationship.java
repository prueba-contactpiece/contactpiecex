
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class EmploymentRelationship {

    protected OptionsList type;
    protected Date entryYear;
    protected List<SalaryPayments> lastSalaryPayments;
    protected Job job;
    protected Date firstPaymentDate;

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setType(OptionsList value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad entryYear.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getEntryYear() {
        return entryYear;
    }

    /**
     * Define el valor de la propiedad entryYear.
     * 
     * @param value
     *     allowed object is
     *     {@link Date }
     *     
     */
    public void setEntryYear(Date value) {
        this.entryYear = value;
    }

    /**
     * Gets the value of the lastSalaryPayments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lastSalaryPayments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLastSalaryPayments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalaryPayments }
     * 
     * 
     */
    public List<SalaryPayments> getLastSalaryPayments() {        
        return this.lastSalaryPayments;
    }

    /**
     * Obtiene el valor de la propiedad job.
     * 
     * @return
     *     possible object is
     *     {@link Job }
     *     
     */
    public Job getJob() {
        return job;
    }

    /**
     * Define el valor de la propiedad job.
     * 
     * @param value
     *     allowed object is
     *     {@link Job }
     *     
     */
    public void setJob(Job value) {
        this.job = value;
    }

    /**
     * Obtiene el valor de la propiedad firstPaymentDate.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getFirstPaymentDate() {
        return firstPaymentDate;
    }

    /**
     * Define el valor de la propiedad firstPaymentDate.
     * 
     * @param value
     *     allowed object is
     *     {@link Date }
     *     
     */
    public void setFirstPaymentDate(Date value) {
        this.firstPaymentDate = value;
    }

}
