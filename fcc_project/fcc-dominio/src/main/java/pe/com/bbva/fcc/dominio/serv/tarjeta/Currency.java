package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Currency {

	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
		
	protected String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String currency;
	public String getCurrency() {return currency;}
	public void setCurrency(String currency) {this.currency = currency;}

	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String isMajor;
	public String getIsMajor() {return isMajor;}
	public void setIsMajor(String isMajor) {this.isMajor = isMajor;}
	
	
}