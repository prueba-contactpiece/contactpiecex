package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ProductConfiguration {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String id;
    public String getId() { return id; }
    public void setId(String value) { this.id = value; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected String description;
	public String getDescription() {return description;	}
	public void setDescription(String description) {this.description = description;	}
    
}
