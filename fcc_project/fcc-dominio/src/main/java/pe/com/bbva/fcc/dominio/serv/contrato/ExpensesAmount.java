package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ExpensesAmount {
	
	protected List<DetailedExpenses> detailedExpenses;
	public List<DetailedExpenses> getDetailedExpenses() { return detailedExpenses; }
	public void setDetailedExpenses(List<DetailedExpenses> detailedExpenses) { this.detailedExpenses = detailedExpenses;}
	
}