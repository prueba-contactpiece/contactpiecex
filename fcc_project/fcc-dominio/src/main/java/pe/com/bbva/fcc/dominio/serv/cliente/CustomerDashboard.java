
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CustomerDashboard {

    protected Customer customer;
    protected Manager manager;
    protected List<Program> programs;
    protected CustomerManagementData customerManagementData;
    protected String managerManagementUnit;
    
    /**
     * Obtiene el valor de la propiedad managerManagementUnit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManagerManagementUnit() {
		return managerManagementUnit;
	}

    /**
     * Define el valor de la propiedad managerManagementUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
	public void setManagerManagementUnit(String managerManagementUnit) {
		this.managerManagementUnit = managerManagementUnit;
	}

	/**
     * Obtiene el valor de la propiedad customer.
     * 
     * @return
     *     possible object is
     *     {@link Customer }
     *     
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Define el valor de la propiedad customer.
     * 
     * @param value
     *     allowed object is
     *     {@link Customer }
     *     
     */
    public void setCustomer(Customer value) {
        this.customer = value;
    }

    /**
     * Obtiene el valor de la propiedad manager.
     * 
     * @return
     *     possible object is
     *     {@link Manager }
     *     
     */
    public Manager getManager() {
        return manager;
    }

    /**
     * Define el valor de la propiedad manager.
     * 
     * @param value
     *     allowed object is
     *     {@link Manager }
     *     
     */
    public void setManager(Manager value) {
        this.manager = value;
    }

    /**
     * Gets the value of the programs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the programs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrograms().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Program }
     * 
     * 
     */
    public List<Program> getPrograms() {
        return this.programs;
    }    
    /**
     * Obtiene el valor de la propiedad customerManagementData.
     * @return
     *     possible object is
     *     {@link CustomerManagementData }
     */
	public CustomerManagementData getCustomerManagementData() {
		return customerManagementData;
	}
	/**
     * Define el valor de la propiedad customerManagementData.
     * @param value
     *     allowed object is
     *     {@link CustomerManagementData }
     */
	public void setCustomerManagementData(CustomerManagementData customerManagementData) {
		this.customerManagementData = customerManagementData;
	} 

}
