package pe.com.bbva.fcc.dominio.serv.tarjeta;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import pe.com.bbva.fcc.dominio.serv.Pagination;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ListCardsData {

	protected List<Card> data;
	public List<Card> getData() { return data; }
	public void setData(List<Card> data) { this.data = data; }
	
	protected Pagination pagination;	
	public Pagination getPagination() { return pagination; }
	public void setPagination(Pagination pagination) { this.pagination = pagination; }	
	
}
