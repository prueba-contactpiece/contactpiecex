package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DetailTaxes {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String description;
	public String getDescription() { return description; }
	public void setDescription(String description) { this.description = description; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Amount amount;
	public Amount getAmount() { return amount; }
	public void setAmount(Amount amount) { this.amount = amount; }

}