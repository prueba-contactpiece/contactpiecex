package pe.com.bbva.fcc.dominio.serv.tarjeta;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown=true)
public class Product {
	
	protected String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	protected List <Type> CommercialClassifications;
	public List<Type> getCommercialClassifications() {return CommercialClassifications;	}
	public void setCommercialClassifications(List<Type> commercialClassifications) {CommercialClassifications = commercialClassifications;}

}
