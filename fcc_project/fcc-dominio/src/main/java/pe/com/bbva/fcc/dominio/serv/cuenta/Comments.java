package pe.com.bbva.fcc.dominio.serv.cuenta;

public class Comments {

	protected Boolean secret;

    /**
     * Obtiene el valor de la propiedad secret.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Boolean getSecret() {
        return secret;
    }

    /**
     * Define el valor de la propiedad secret.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecret(Boolean value) {
        this.secret = value;
    }
}
