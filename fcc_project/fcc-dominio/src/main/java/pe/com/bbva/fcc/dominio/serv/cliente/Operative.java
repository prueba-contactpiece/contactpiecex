package pe.com.bbva.fcc.dominio.serv.cliente;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Operative {
	
	protected String operative;
	protected OptionsList types;
	protected OptionsList status;
	protected OptionsList name;
	
	 /**
     * Obtiene el valor de la propiedad operative. 
     * @return
     *     possible object is
     *     {@link String }
     */
	public String getOperative() {
		return operative;
	}
	/**
     * Define el valor de la propiedad operative.
     * @param value
     *     allowed object is
     *     {@link String }
     */
	public void setOperative(String operative) {
		this.operative = operative;
	}
	/**
     * Obtiene el valor de la propiedad types. 
     * @return
     *     possible object is
     *     {@link OptionsList }
     */
	public OptionsList getTypes() {
		return types;
	}
	/**
     * Define el valor de la propiedad types.
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     */
	public void setTypes(OptionsList types) {
		this.types = types;
	}
	/**
     * Obtiene el valor de la propiedad status. 
     * @return
     *     possible object is
     *     {@link OptionsList }
     */
	public OptionsList getStatus() {
		return status;
	}
	/**
     * Define el valor de la propiedad status.
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     */
	public void setStatus(OptionsList status) {
		this.status = status;
	}
	/**
     * Obtiene el valor de la propiedad name. 
     * @return
     *     possible object is
     *     {@link OptionsList }
     */
	public OptionsList getName() {
		return name;
	}
	/**
     * Define el valor de la propiedad name.
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     */
	public void setName(OptionsList name) {
		this.name = name;
	}

}
