package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RelatedContracts {

	protected ProductProposal product;
	public ProductProposal getProduct() { return product; }
	public void setProduct(ProductProposal product) { this.product = product; }	
}
