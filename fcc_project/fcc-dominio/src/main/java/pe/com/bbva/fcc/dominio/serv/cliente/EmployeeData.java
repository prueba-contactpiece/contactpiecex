
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class EmployeeData {

    protected OptionsList type;
    protected Bank bank;
    protected OptionsList customerPlanStatus;
    protected OptionsList customerPlanType;
    protected Boolean receiveBankPayments;
    protected List<EmploymentRelationship> employmentRelationship;
    protected boolean isBankemployee;

    /**
     * Obtiene el valor de la propiedad type.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getType() {
        return type;
    }

    /**
     * Define el valor de la propiedad type.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setType(OptionsList value) {
        this.type = value;
    }

    /**
     * Obtiene el valor de la propiedad bank.
     * 
     * @return
     *     possible object is
     *     {@link Bank }
     *     
     */
    public Bank getBank() {
        return bank;
    }

    /**
     * Define el valor de la propiedad bank.
     * 
     * @param value
     *     allowed object is
     *     {@link Bank }
     *     
     */
    public void setBank(Bank value) {
        this.bank = value;
    }

    /**
     * Obtiene el valor de la propiedad customerPlanStatus.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getCustomerPlanStatus() {
        return customerPlanStatus;
    }

    /**
     * Define el valor de la propiedad customerPlanStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setCustomerPlanStatus(OptionsList value) {
        this.customerPlanStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad customerPlanType.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getCustomerPlanType() {
        return customerPlanType;
    }

    /**
     * Define el valor de la propiedad customerPlanType.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setCustomerPlanType(OptionsList value) {
        this.customerPlanType = value;
    }

    /**
     * Obtiene el valor de la propiedad receiveBankPayments.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReceiveBankPayments() {
        return receiveBankPayments;
    }

    /**
     * Define el valor de la propiedad receiveBankPayments.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReceiveBankPayments(Boolean value) {
        this.receiveBankPayments = value;
    }

    /**
     * Gets the value of the employmentRelationship property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the employmentRelationship property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmploymentRelationship().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmploymentRelationship }
     * 
     * 
     */
    public List<EmploymentRelationship> getEmploymentRelationship() {
        return this.employmentRelationship;
    }

    /**
     * Obtiene el valor de la propiedad isBankemployee.
     * 
     */
    public boolean isIsBankemployee() {
        return isBankemployee;
    }

    /**
     * Define el valor de la propiedad isBankemployee.
     * 
     */
    public void setIsBankemployee(boolean value) {
        this.isBankemployee = value;
    }

}
