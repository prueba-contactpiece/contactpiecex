
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Program {

    protected String id;
    protected String description;
    protected OptionsList type;
    protected BigDecimal availableLoyaltyUnitsBalance;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }
    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }
    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }
    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }
    /**
     * Obtiene el valor de la propiedad availableLoyaltyUnitsBalance.
     * @return
     *     possible object is
     *     {@link BigDecimal }
     */
	public BigDecimal getAvailableLoyaltyUnitsBalance() {
		return availableLoyaltyUnitsBalance;
	}	
	/**
     * Define el valor de la propiedad availableLoyaltyUnitsBalance.
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     */
	public void setAvailableLoyaltyUnitsBalance(
			BigDecimal availableLoyaltyUnitsBalance) {
		this.availableLoyaltyUnitsBalance = availableLoyaltyUnitsBalance;
	}
	/**
     * Obtiene el valor de la propiedad type.
     * @return
     *     possible object is
     *     {@link OptionsList }
     */
	public OptionsList getType() {
		return type;
	}
	/**
     * Define el valor de la propiedad type.
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     */
	public void setType(OptionsList type) {
		this.type = type;
	}
}
