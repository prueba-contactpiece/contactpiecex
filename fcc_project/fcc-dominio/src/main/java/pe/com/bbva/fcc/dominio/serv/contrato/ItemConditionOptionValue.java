package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ItemConditionOptionValue {

	protected String value;
	public String getValue() { return value; }
	public void setValue(String value) { this.value = value; }
	
	protected String minimum;
	public String getMinimum() { return minimum; }
	public void setMinimum(String minimum) { this.minimum = minimum; }
	
	protected String maximum;
	public String getMaximum() { return maximum; }
	public void setMaximum(String maximum) { this.maximum = maximum; }
	
	protected String formatId;
	public String getFormatId() { return formatId; }
	public void setFormatId(String formatId) { this.formatId = formatId; }
	
	private Boolean isSelected;
	public Boolean getIsSelected() { return isSelected; }
	public void setIsSelected(Boolean isSelected) { this.isSelected = isSelected; }
}