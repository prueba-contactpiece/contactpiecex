package pe.com.bbva.fcc.dominio.serv.tarjeta;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class Limits {
	protected List<Limit> limits;
	public List<Limit> getLimits() { return limits; }
	public void setLimits(List<Limit> limits) { this.limits = limits; }
}
