
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SalaryPayments {

    protected Date period;
    protected Money salaryAmount;
    protected OptionsList category;
    protected Company company;

    /**
     * Obtiene el valor de la propiedad period.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getPeriod() {
        return period;
    }

    /**
     * Define el valor de la propiedad period.
     * 
     * @param value
     *     allowed object is
     *     {@link Date }
     *     
     */
    public void setPeriod(Date value) {
        this.period = value;
    }

    /**
     * Obtiene el valor de la propiedad salaryAmount.
     * 
     * @return
     *     possible object is
     *     {@link Money }
     *     
     */
    public Money getSalaryAmount() {
        return salaryAmount;
    }

    /**
     * Define el valor de la propiedad salaryAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Money }
     *     
     */
    public void setSalaryAmount(Money value) {
        this.salaryAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad category.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getCategory() {
        return category;
    }

    /**
     * Define el valor de la propiedad category.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setCategory(OptionsList value) {
        this.category = value;
    }

    /**
     * Obtiene el valor de la propiedad company.
     * 
     * @return
     *     possible object is
     *     {@link Company }
     *     
     */
    public Company getCompany() {
        return company;
    }

    /**
     * Define el valor de la propiedad company.
     * 
     * @param value
     *     allowed object is
     *     {@link Company }
     *     
     */
    public void setCompany(Company value) {
        this.company = value;
    }

}
