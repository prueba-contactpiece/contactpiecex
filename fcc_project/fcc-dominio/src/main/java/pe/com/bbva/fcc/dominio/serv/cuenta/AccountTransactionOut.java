package pe.com.bbva.fcc.dominio.serv.cuenta;

import java.util.List;

import pe.com.bbva.fcc.dominio.serv.Pagination;


public class AccountTransactionOut {
	protected List<AccountTransaction> items;
	public List<AccountTransaction> getItems() { return items; }
	public void setItems(List<AccountTransaction> items) { this.items = items; }
	
	protected Pagination pagination;	
	public Pagination getPagination() { return pagination; }
	public void setPagination(Pagination pagination) { this.pagination = pagination; }	
}
