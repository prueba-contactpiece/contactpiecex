
package pe.com.bbva.fcc.dominio.serv.cliente;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pe.com.bbva.fcc.dominio.serv.Pagination;

@JsonIgnoreProperties(ignoreUnknown=true)

public class Customer extends Person {
	
    protected Branch branch;
    protected EmployeeData employeeData;
    protected DelinquencyRiskData delinquencyRiskData;
    protected OptionsList groupRelationshipStatus;
    protected Date membershipDate;
    protected boolean allowsAd;
    protected List<Document> documents;
    protected Pagination pagination;
    
//    protected String bxiAffiliation;
//    protected String lifePointAffiliation;
//    protected String milesAffiliation;

    /**
     * Obtiene el valor de la propiedad branch.
     * 
     * @return
     *     possible object is
     *     {@link Branch }
     *     
     */
    public Branch getBranch() {
        return branch;
    }

    /**
     * Define el valor de la propiedad branch.
     * 
     * @param value
     *     allowed object is
     *     {@link Branch }
     *     
     */
    public void setBranch(Branch value) {
        this.branch = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeData.
     * 
     * @return
     *     possible object is
     *     {@link EmployeeData }
     *     
     */
    public EmployeeData getEmployeeData() {
        return employeeData;
    }

    /**
     * Define el valor de la propiedad employeeData.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployeeData }
     *     
     */
    public void setEmployeeData(EmployeeData value) {
        this.employeeData = value;
    }

    /**
     * Obtiene el valor de la propiedad delinquencyRiskData.
     * 
     * @return
     *     possible object is
     *     {@link DelinquencyRiskData }
     *     
     */
    public DelinquencyRiskData getDelinquencyRiskData() {
        return delinquencyRiskData;
    }

    /**
     * Define el valor de la propiedad delinquencyRiskData.
     * 
     * @param value
     *     allowed object is
     *     {@link DelinquencyRiskData }
     *     
     */
    public void setDelinquencyRiskData(DelinquencyRiskData value) {
        this.delinquencyRiskData = value;
    }

    /**
     * Obtiene el valor de la propiedad groupRelationshipStatus.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getGroupRelationshipStatus() {
        return groupRelationshipStatus;
    }

    /**
     * Define el valor de la propiedad groupRelationshipStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setGroupRelationshipStatus(OptionsList value) {
        this.groupRelationshipStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad membershipDate.
     * 
     * @return
     *     possible object is
     *     {@link Date }
     *     
     */
    public Date getMembershipDate() {
        return membershipDate;
    }

    /**
     * Define el valor de la propiedad membershipDate.
     * 
     * @param value
     *     allowed object is
     *     {@link Date }
     *     
     */
    public void setMembershipDate(Date value) {
        this.membershipDate = value;
    }

    /**
     * Obtiene el valor de la propiedad allowsAd.
     * 
     */
    public boolean isAllowsAd() {
        return allowsAd;
    }

    /**
     * Define el valor de la propiedad allowsAd.
     * 
     */
    public void setAllowsAd(boolean value) {
        this.allowsAd = value;
    }

    /**
     * Gets the value of the documents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Document }
     * 
     * 
     */
    public List<Document> getDocuments() {
        return this.documents;
    }

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
}
