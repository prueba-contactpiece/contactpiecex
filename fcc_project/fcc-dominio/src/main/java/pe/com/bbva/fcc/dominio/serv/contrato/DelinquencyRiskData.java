
package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DelinquencyRiskData {

    protected OptionsList insolvencyStatus;
    protected OptionsList bankDefaultStatus;
    protected OptionsList groupDefaultStatus;
    protected List<RatingClassification> ratingClassification;
    protected Integer debtPercentage;
    protected List<DelinquencyIneligibleData> ineligibleCodes;    
    protected RiskBankClassification riskBankClassification;
    protected SbsClassification sbsClassification;
    
    /**
     * Obtiene el valor de la propiedad insolvencyStatus.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getInsolvencyStatus() {
        return insolvencyStatus;
    }

    /**
     * Define el valor de la propiedad insolvencyStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setInsolvencyStatus(OptionsList value) {
        this.insolvencyStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad bankDefaultStatus.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getBankDefaultStatus() {
        return bankDefaultStatus;
    }

    /**
     * Define el valor de la propiedad bankDefaultStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setBankDefaultStatus(OptionsList value) {
        this.bankDefaultStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad groupDefaultStatus.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getGroupDefaultStatus() {
        return groupDefaultStatus;
    }

    /**
     * Define el valor de la propiedad groupDefaultStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setGroupDefaultStatus(OptionsList value) {
        this.groupDefaultStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad debtPercentage.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDebtPercentage() {
        return debtPercentage;
    }

    /**
     * Define el valor de la propiedad debtPercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDebtPercentage(Integer value) {
        this.debtPercentage = value;
    }

    /**
     * Gets the value of the ineligibleCodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ineligibleCodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIneligibleCodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OptionsList }
     * 
     * 
     */
    public List<DelinquencyIneligibleData> getIneligibleCodes() {
        return this.ineligibleCodes;
    }

	public List<RatingClassification> getRatingClassification() {
		return ratingClassification;
	}

	public void setRatingClassification(
			List<RatingClassification> ratingClassification) {
		this.ratingClassification = ratingClassification;
	}

	public void setIneligibleCodes(List<DelinquencyIneligibleData> ineligibleCodes) {
		this.ineligibleCodes = ineligibleCodes;
	}

	public RiskBankClassification getRiskBankClassification() {
		return riskBankClassification;
	}

	public void setRiskBankClassification(RiskBankClassification riskBankClassification) {
		this.riskBankClassification = riskBankClassification;
	}

	public SbsClassification getSbsClassification() {
		return sbsClassification;
	}

	public void setSbsClassification(SbsClassification sbsClassification) {
		this.sbsClassification = sbsClassification;
	}
}
