
package pe.com.bbva.fcc.dominio.serv.cliente;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PersonExtendedData {

    protected String sex;
    protected Address birthPlace;
    protected OptionsList maritalStatus;
    protected OptionsList maritalRegime;
    protected OptionsList educationLevel;
    protected OptionsList prefix;
    protected Document signature;
    protected OptionsList inhabitantResidenceCountry;
    protected OptionsList inhabitantCountry;    
    protected Integer childrenNumber;
    protected String interiorNumber;

    /**
     * Obtiene el valor de la propiedad sex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Define el valor de la propiedad sex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }    

    /**
     * Obtiene el valor de la propiedad birthPlace.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getBirthPlace() {
        return birthPlace;
    }

    /**
     * Define el valor de la propiedad birthPlace.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setBirthPlace(Address value) {
        this.birthPlace = value;
    }

    /**
     * Obtiene el valor de la propiedad maritalStatus.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Define el valor de la propiedad maritalStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setMaritalStatus(OptionsList value) {
        this.maritalStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad maritalRegime.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getMaritalRegime() {
        return maritalRegime;
    }

    /**
     * Define el valor de la propiedad maritalRegime.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setMaritalRegime(OptionsList value) {
        this.maritalRegime = value;
    }

    /**
     * Obtiene el valor de la propiedad educationLevel.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getEducationLevel() {
        return educationLevel;
    }

    /**
     * Define el valor de la propiedad educationLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setEducationLevel(OptionsList value) {
        this.educationLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad prefix.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getPrefix() {
        return prefix;
    }

    /**
     * Define el valor de la propiedad prefix.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setPrefix(OptionsList value) {
        this.prefix = value;
    }

    /**
     * Obtiene el valor de la propiedad signature.
     * 
     * @return
     *     possible object is
     *     {@link Document }
     *     
     */
    public Document getSignature() {
        return signature;
    }

    /**
     * Define el valor de la propiedad signature.
     * 
     * @param value
     *     allowed object is
     *     {@link Document }
     *     
     */
    public void setSignature(Document value) {
        this.signature = value;
    }

    /**
     * Obtiene el valor de la propiedad inhabitantResidenceCountry.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getInhabitantResidenceCountry() {
        return inhabitantResidenceCountry;
    }

    /**
     * Define el valor de la propiedad inhabitantResidenceCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setInhabitantResidenceCountry(OptionsList value) {
        this.inhabitantResidenceCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad inhabitantCountry.
     * 
     * @return
     *     possible object is
     *     {@link OptionsList }
     *     
     */
    public OptionsList getInhabitantCountry() {
        return inhabitantCountry;
    }

    /**
     * Define el valor de la propiedad inhabitantCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link OptionsList }
     *     
     */
    public void setInhabitantCountry(OptionsList value) {
        this.inhabitantCountry = value;
    }
    /**
     * Obtiene el valor de la propiedad childrenNumber.
     * @return
     *     possible object is
     *     {@link Integer }
     */
	public Integer getChildrenNumber() {
		return childrenNumber;
	}
	/**
     * Define el valor de la propiedad childrenNumber.
     * @param value
     *     allowed object is
     *     {@link Integer }
     */
	public void setChildrenNumber(Integer childrenNumber) {
		this.childrenNumber = childrenNumber;
	}
	/**
     * Obtiene el valor de la propiedad interiorNumber.
     * @return
     *     possible object is
     *     {@link String }
     */
	public String getInteriorNumber() {
		return interiorNumber;
	}
	/**
     * Define el valor de la propiedad interiorNumber.
     * @param value
     *     allowed object is
     *     {@link String }
     */
	public void setInteriorNumber(String interiorNumber) {
		this.interiorNumber = interiorNumber;
	}

}
