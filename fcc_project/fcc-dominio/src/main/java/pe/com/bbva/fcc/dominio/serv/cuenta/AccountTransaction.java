package pe.com.bbva.fcc.dominio.serv.cuenta;


import java.sql.Timestamp;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class AccountTransaction {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Type type;
	public Type getType() { return type; }
	public void setType(Type type) { this.type = type; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Concept concept;
	public Concept getConcept() { return concept; }
	public void setConcept(Concept concept) { this.concept = concept; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Timestamp valueDate;
	public Timestamp getValueDate() { return valueDate; }
	public void setValueDate(Timestamp valueDate) { this.valueDate = valueDate; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Timestamp transactionDate;
	public Timestamp transactionDate() { return transactionDate; }
	public void setTransactionDate(Timestamp transactionDate) { this.transactionDate = transactionDate; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Amount amount;
	public Amount getAmount() { return amount; }
	public void setAmount(Amount amount) { this.amount = amount; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Taxes taxes;
	public Taxes getTaxes() { return taxes; }
	public void setAmount(Taxes taxes) { this.taxes = taxes; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Scheme scheme;
	public Scheme getScheme() { return scheme; }
	public void setScheme(Scheme scheme) { this.scheme = scheme; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Contract contract;
	public Contract getContract() { return contract; }
	public void setContract(Contract contract) { this.contract= contract; }
	
	
	
	
}



