package pe.com.bbva.fcc.dominio.serv.cliente;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ManagementData {
	
	protected Boolean allowAlerts;
	protected Boolean personalDataDisclosure;
	protected PersonalDataDisclosureIndicator personalDataDisclosureIndicator; 
	
	/**
	* 
	* @return
	* The allowAlerts
	*/
	public Boolean getAllowAlerts() {
		return allowAlerts;
	}
	/**
	* 
	* @param allowAlerts
	* The allowAlerts
	*/
	public void setAllowAlerts(Boolean allowAlerts) {
		this.allowAlerts = allowAlerts;
	}
	public Boolean getPersonalDataDisclosure() {
		return personalDataDisclosure;
	}
	public void setPersonalDataDisclosure(Boolean personalDataDisclosure) {
		this.personalDataDisclosure = personalDataDisclosure;
	}
	/**
	* 
	* @param personalDataDisclosureIndicator
	* The personalDataDisclosureIndicator
	*/
	public PersonalDataDisclosureIndicator getPersonalDataDisclosureIndicator() {
		return personalDataDisclosureIndicator;
	}
	public void setPersonalDataDisclosureIndicator(PersonalDataDisclosureIndicator personalDataDisclosureIndicator) {
		this.personalDataDisclosureIndicator = personalDataDisclosureIndicator;
	}
	
	
}
