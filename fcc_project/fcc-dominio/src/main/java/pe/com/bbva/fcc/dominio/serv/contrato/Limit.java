package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Limit {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money currentValue;
	public Money getCurrentValue() { return currentValue; }
	public void setCurrentValue(Money currentValue) { this.currentValue = currentValue; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money availableBalance;
	public Money getAvailableBalance() { return availableBalance; }
	public void setAvailableBalance(Money availableBalance) { this.availableBalance = availableBalance; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String id;
	public String getId() {		return id;	}
	public void setId(String id) {		this.id = id;	}
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Amount> amounts;
	public List<Amount> getAmounts() {		return amounts;	}
	public void setAmounts(List<Amount> amounts) {		this.amounts = amounts;	}
	

}
