package pe.com.bbva.fcc.dominio.serv.contrato;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Period {
	protected BigDecimal amount;
	public BigDecimal getAmount() { return amount; }
	public void setAmount(BigDecimal amount) { this.amount = amount; }
	
	protected OptionsList timeUnit;
    public OptionsList getTimeUnit() { return timeUnit; }
	public void setTimeUnit(OptionsList timeUnit) { this.timeUnit = timeUnit; }
}