package pe.com.bbva.fcc.dominio.serv.contrato;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SbsClassification {
	
	protected Date classificationPeriod;
	protected BigDecimal normalClassificationPercent;
	protected BigDecimal potentialProblemClassificationPercent;
	protected BigDecimal deficientClassificationPercent;
	protected BigDecimal questionableClassificationPercent;
	protected BigDecimal lostClassificationPercent;
	
	public Date getClassificationPeriod() {
		return classificationPeriod;
	}
	public void setClassificationPeriod(Date classificationPeriod) {
		this.classificationPeriod = classificationPeriod;
	}
	public BigDecimal getNormalClassificationPercent() {
		return normalClassificationPercent;
	}
	public void setNormalClassificationPercent(
			BigDecimal normalClassificationPercent) {
		this.normalClassificationPercent = normalClassificationPercent;
	}
	public BigDecimal getPotentialProblemClassificationPercent() {
		return potentialProblemClassificationPercent;
	}
	public void setPotentialProblemClassificationPercent(
			BigDecimal potentialProblemClassificationPercent) {
		this.potentialProblemClassificationPercent = potentialProblemClassificationPercent;
	}
	public BigDecimal getDeficientClassificationPercent() {
		return deficientClassificationPercent;
	}
	public void setDeficientClassificationPercent(
			BigDecimal deficientClassificationPercent) {
		this.deficientClassificationPercent = deficientClassificationPercent;
	}
	public BigDecimal getQuestionableClassificationPercent() {
		return questionableClassificationPercent;
	}
	public void setQuestionableClassificationPercent(
			BigDecimal questionableClassificationPercent) {
		this.questionableClassificationPercent = questionableClassificationPercent;
	}
	public BigDecimal getLostClassificationPercent() {
		return lostClassificationPercent;
	}
	public void setLostClassificationPercent(BigDecimal lostClassificationPercent) {
		this.lostClassificationPercent = lostClassificationPercent;
	}

}
