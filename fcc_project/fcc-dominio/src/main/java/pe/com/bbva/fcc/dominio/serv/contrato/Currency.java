package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Currency {

	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	protected String code;
	public String getCode() { return code; }
	public void setCode(String code) { this.code = code; }
	
	protected String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
}