package pe.com.bbva.fcc.dominio.serv.contrato;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ContractAction {
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected ProductProposal product;
	public ProductProposal getProduct() { return product; }
	public void setProduct(ProductProposal product) { this.product = product; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Currency currency;
	public Currency getCurrency() { return currency; }
	public void setCurrency(Currency currency) { this.currency = currency; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Branch branch;
	public Branch getBranch() { return branch; }
	public void setBranch(Branch branch) { this.branch = branch; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Status status;
	public Status getStatus() { return status; }
	public void setStatus(Status status) { this.status = status; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected User userCustomization;
	public User getUserCustomization() { return userCustomization; }
	public void setUserCustomization(User userCustomization) { this.userCustomization = userCustomization; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected DelinquencyContract  delinquency;
	public DelinquencyContract getDelinquency() { return delinquency; }
	public void setDelinquency(DelinquencyContract delinquency) { this.delinquency = delinquency; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected NextFractionInformation nextFractionInformation;
	public NextFractionInformation getNextFractionInformation() { return nextFractionInformation; }
    public void setNextFractionInformation(NextFractionInformation nextFractionInformation) { this.nextFractionInformation = nextFractionInformation; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money fractionAmount;
	public Money getFractionAmount() { return fractionAmount; }
    public void setFractionAmount(Money fractionAmount) { this.fractionAmount = fractionAmount; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money totalAmount;
	public Money getTotalAmount() { return totalAmount; }
    public void setTotalAmount(Money totalAmount) { this.totalAmount = totalAmount; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money unusedBalance;
	public Money getUnusedBalance() { return unusedBalance; }
    public void setUnusedBalance(Money unusedBalance) { this.unusedBalance = unusedBalance; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected List<RelatedContracts> relatedContracts;
	public List<RelatedContracts> getRelatedContracts() { return relatedContracts; }
    public void setRelatedContracts(List<RelatedContracts> relatedContracts) { this.relatedContracts = relatedContracts; }
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
   	protected Date  openingDate;
   	public Date getOpeningDate() { return openingDate; }
   	public void setOpeningDate(Date openingDate) { this.openingDate = openingDate; }
   	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
   	protected Date  paymentDate;
   	public Date getPaymentDate() { return paymentDate; }
   	public void setPaymentDate(Date paymentDate) { this.paymentDate = paymentDate; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date  expirationDate;
	public Date getExpirationDate() { return expirationDate; }
	public void setExpirationDate(Date expirationDate) { this.expirationDate = expirationDate; }
   
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  managementEntity;
	public String getManagementEntity() { return managementEntity; }
	public void setManagementEntity(String managementEntity) { this.managementEntity = managementEntity; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Condition  conditions;
	public Condition getConditions() { return conditions; }
	public void setConditions(Condition conditions) { this.conditions = conditions; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money availableBalance;
	public Money getAvailableBalance() { return availableBalance; }
	public void setAvailableBalance(Money availableBalance) { this.availableBalance = availableBalance; }
	    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected HoldBacks holdBacks;
	public HoldBacks getHoldBacks() { return holdBacks; }
	public void setHoldBanks(HoldBacks holdBacks) { this.holdBacks = holdBacks; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Limits limits;
	public Limits getLimits() { return limits; }
	public void setLimits(Limits limits) { this.limits = limits; }
	   
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money policyAmount;
	public Money getPolicyAmount() { return policyAmount; }
	public void setPolicyAmount(Money policyAmount) { this.policyAmount = policyAmount; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Interest> interests;
	public List<Interest> getInterests() { return interests; }
	public void setInterests(List<Interest> interests) { this.interests = interests; }	
	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money amount;
	public Money getAmount() { return amount; }
	public void setAmount(Money amount) { this.amount = amount; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money totalCreditLine;
	public Money getTotalCreditLine() { return totalCreditLine; }
	public void setTotalCreditLine(Money totalCreditLine) { this.totalCreditLine = totalCreditLine; }
	  
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Period periodicity;
	public Period getPeriodicity() { return periodicity; }
    public void setPeriodicity(Period periodicity) { this.periodicity = periodicity; }
   
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  installmentTotalCount;
	public String getInstallmentTotalCount() { return installmentTotalCount; }
	public void setInstallmentTotalCount(String installmentTotalCount) { this.installmentTotalCount = installmentTotalCount; }
	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date  nextInstallmentInformation;
	public Date getNextInstallmentInformation() { return nextInstallmentInformation; }
	public void setNextInstallmentInformation(Date nextInstallmentInformation) { this.nextInstallmentInformation = nextInstallmentInformation; }
  
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money capitalTotalAmount;
	public Money getCapitalTotalAmount() { return capitalTotalAmount; }
	public void setCapitalTotalAmount(Money capitalTotalAmount) { this.capitalTotalAmount = capitalTotalAmount; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money installmentAmount;
	public Money getInstallmentAmount() { return installmentAmount; }
	public void setInstallmentAmount(Money installmentAmount) { this.installmentAmount = installmentAmount; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected AcquisitionInformation acquisitionInformation;
	public AcquisitionInformation getAcquisitionInformation() { return acquisitionInformation; }
	public void setAcquisitionInformation(AcquisitionInformation acquisitionInformation) { this.acquisitionInformation = acquisitionInformation; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Interest> interest;
	public List<Interest> getInterest() { return interest; }
	public void setInterest(List<Interest> interest) { this.interest = interest; }	

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected OptionsList scheduleType;
	public OptionsList getScheduleType() { return scheduleType; }
	public void setScheduleType(OptionsList scheduleType) { this.scheduleType = scheduleType; }	

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String isRefinanced ;
	public String getiIsRefinanced() { return isRefinanced; }
	public void setIsRefinanced(String isRefinanced) { this.isRefinanced = isRefinanced; }	

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money balance;
	public Money getBalance() { return balance; }
	public void setBalance(Money balance) { this.balance = balance; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected OptionsList ownershipType;
	public OptionsList getOwnershipType() { return ownershipType; }
    public void setOwnershipType(OptionsList ownershipType) { this.ownershipType = ownershipType; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Retention> retentions;
	public List<Retention> getRetentions() { return retentions; }
	public void setRetentions(List<Retention> retentions) { this.retentions = retentions; }
  
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money overdraftAmount;
	public Money getOverdraftAmount() { return overdraftAmount; }
	public void setOverdraftAmount(Money overdraftAmount) { this.overdraftAmount = overdraftAmount; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Indicators indicators;
	public Indicators getIndicators() { return indicators; }
    public void setIndicators(Indicators indicators) { this.indicators = indicators; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money creditLine;
	public Money getCreditLine() { return creditLine; }
	public void setCreditLine(Money creditLine) { this.creditLine = creditLine; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money guaranteeAmount;
	public Money getGuaranteeAmount() { return guaranteeAmount; }
	public void setGuaranteeAmount(Money guaranteeAmount) { this.guaranteeAmount = guaranteeAmount; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  nextPaymetDate;
	public String getNextPaymetDate() { return nextPaymetDate; }
	public void setNextPaymetDate(String nextPaymetDate) { this.nextPaymetDate = nextPaymetDate; }
   
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money approvedLine;
	public Money getApprovedLine() { return approvedLine; }
	public void setApprovedLine(Money approvedLine) { this.approvedLine = approvedLine; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money availableLine;
	public Money getAvailableLine() { return availableLine; }
	public void setAvailableLine(Money availableLine) { this.availableLine = availableLine; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected BigDecimal  pastDuePercentage;
	public BigDecimal getPastDuePercentage() { return pastDuePercentage; }
	public void setPastDuePercentage(BigDecimal pastDuePercentage) { this.pastDuePercentage = pastDuePercentage; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected BigDecimal  pendingPayment;
	public BigDecimal getpendingPayment() { return pendingPayment; }
	public void setPendingPayment(BigDecimal pendingPayment) { this.pendingPayment = pendingPayment; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected BigDecimal  activePercentage;
	public BigDecimal getActivePercentage() { return activePercentage; }
	public void setActivePercentage(BigDecimal activePercentage) { this.activePercentage = activePercentage; }

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money awardedAmount;
	public Money getAwardedAmount() { return awardedAmount; }
	public void setAwardedAmount(Money awardedAmount) { this.awardedAmount = awardedAmount; }
	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected BigDecimal  currentPercentage;
	public BigDecimal getCurrentPercentage() { return currentPercentage; }
	public void setCurrentPercentage(BigDecimal currentPercentage) { this.currentPercentage = currentPercentage; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money retainAmount;
	public Money getRetainAmount() { return retainAmount; }
	public void setRetainAmount(Money retainAmount) { this.retainAmount = retainAmount; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money pendingAmount;
	public Money getPendingAmount() { return pendingAmount; }
	public void setPendingAmount(Money pendingAmount) { this.pendingAmount = pendingAmount; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected  Installments installments;
	public  Installments getInstallments() { return installments; }
    public void setInstallments(Installments installments) { this.installments = installments; }
    
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected  AmortizationSchedule amortizationSchedule;
	public  AmortizationSchedule getAmortizationSchedule() { return amortizationSchedule; }
    public void setAmortizationSchedule(AmortizationSchedule amortizationSchedule) { this.amortizationSchedule = amortizationSchedule; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
 	protected String  date;
 	public String getDate() { return date; }
 	public void setnDate(String date) { this.date = date; }
 	
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
 	protected Date  nextPaymentDate;
 	public Date getNextPaymentDate() { return nextPaymentDate; }
 	public void setNextPaymentDate(Date nextPaymentDate) { this.nextPaymentDate = nextPaymentDate; } 	
 	
 	@JsonInclude(JsonInclude.Include.NON_EMPTY)
 	protected Money originalAmount;
 	public Money getOriginalAmount() { return originalAmount; }
 	public void setOriginalAmount(Money originalAmount) { this.originalAmount = originalAmount; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  policyCode;
	public String getPolicyCode() { return policyCode; }
	public void setPolicyCode(String policyCode) { this.policyCode = policyCode; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected OptionsList  modalityPlan;
	public OptionsList getModalityPlan() { return modalityPlan; }
	public void setModalityPlan(OptionsList modalityPlan) { this.modalityPlan = modalityPlan; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  isInsuredBeneficiary;
	public String getIsInsuredBeneficiary() { return isInsuredBeneficiary; }
	public void setIsInsuredBeneficiary(String isInsuredBeneficiary) { this.isInsuredBeneficiary = isInsuredBeneficiary; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String  hasDebt;
	public String getHasDebt() { return hasDebt; }
	public void setHasDebt(String hasDebt) { this.hasDebt = hasDebt; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date  coverStartDate;
	public Date getCoverStartDate() { return coverStartDate; }
	public void setCoverStartDate(Date coverStartDate) { this.coverStartDate = coverStartDate; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Date  coverEndDate;
	public Date getCoverEndDate() { return coverEndDate; }
	public void setCoverEndDate(Date coverEndDate) { this.coverEndDate = coverEndDate; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money  premium;
	public Money getPremium() { return premium; }
	public void setPremium(Money premium) { this.premium = premium; }
   
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Money  delinquencyAmount;
	public Money getDelinquencyAmount() { return delinquencyAmount; }
	public void setDelinquencyAmount(Money delinquencyAmount) { this.delinquencyAmount = delinquencyAmount; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected OptionsList  manager;
	public OptionsList getManager() { return manager; }
	public void setManager(OptionsList manager) { this.manager = manager; } 
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Participant>  participants;
	public List<Participant>  getParticipants() { return participants; }
	public void setParticipants(List<Participant>  participants) { this.participants = participants; } 
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money outstandingDebt;
	public Money getOutstandingDebt() { return outstandingDebt; }
    public void setOutstandingDebt(Money outstandingDebt) { this.outstandingDebt = outstandingDebt; }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    protected Money totalDebt;
	public Money getTotalDebt() { return totalDebt; }
    public void setTotalDebt(Money totalDebt) { this.totalDebt = totalDebt; }

}
