package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DetailedExpenses {
	
	protected OptionsList type;
	public OptionsList getType() { return type; }
	public void setType(OptionsList type) { this.type = type; }
	
	protected Money amount;
	public Money getAmount() { return amount; }
	public void setAmount(Money amount) { this.amount = amount; }	
}