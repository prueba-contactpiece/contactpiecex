package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Scheme {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected subCategory subcategory;
	public subCategory getSubCategory() { return subcategory; }
	public void setSubCategory(subCategory subcategory) { this.subcategory = subcategory; }
	

}