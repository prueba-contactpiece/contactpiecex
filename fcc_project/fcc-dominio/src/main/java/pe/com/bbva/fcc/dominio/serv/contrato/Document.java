
package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Document {

    protected DocumentFile file;

	public DocumentFile getFile() {
		return file;
	}

	public void setFile(DocumentFile file) {
		this.file = file;
	}

}
