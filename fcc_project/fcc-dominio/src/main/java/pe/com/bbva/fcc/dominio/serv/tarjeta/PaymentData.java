package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)

public class PaymentData {
	
	protected Amount amount;
	public Amount getAmount() { return amount; }
	public void setAmount(Amount amount) { this.amount = amount; }
	
	protected Amount amountLocalCurrency;
	public Amount getAmountLocalCurrency() { return amountLocalCurrency; }
	public void setAmountLocalCurrency(Amount amountLocalCurrency) { this.amountLocalCurrency = amountLocalCurrency; }


}
