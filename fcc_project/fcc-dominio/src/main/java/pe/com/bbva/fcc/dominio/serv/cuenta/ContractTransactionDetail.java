package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ContractTransactionDetail {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Amount branch;
	public Amount getBranch() { return branch; }
	public void setBranch(Amount branch) { this.branch = branch; }
	
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Participants participants;
	public Participants getParticipants() { return participants; }
	public void setBranch(Participants participants) { this.participants = participants; }
	
	
	
	
	
}
