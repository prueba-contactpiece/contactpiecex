package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Indicators {
	
	protected String hasPamVisaSecurity;
	public String getHasPamVisaSecurity() {	return hasPamVisaSecurity;}
	public void setHasPamVisaSecurity(String hasPamVisaSecurity) {this.hasPamVisaSecurity = hasPamVisaSecurity;}
	
	protected IsActive isActive;
	public IsActive getIsActive() {return isActive;}
	public void setIsActive(IsActive isActive) {this.isActive = isActive;}
	
	protected IsMainCard isMainCard;
	public IsMainCard getIsMainCard() {return isMainCard;}
	public void setIsMainCard(IsMainCard isMainCard) {this.isMainCard = isMainCard;}

	
}
