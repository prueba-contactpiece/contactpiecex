package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Limits {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected List<Limit> limits;
	public List<Limit> getLimits() { return limits; }
	public void setLimits(List<Limit> limits) { this.limits = limits; }


}
