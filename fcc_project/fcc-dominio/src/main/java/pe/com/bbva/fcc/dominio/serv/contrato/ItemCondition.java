package pe.com.bbva.fcc.dominio.serv.contrato;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ItemCondition {

	private String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	private String commercialName;
	public String getCommercialName() { return commercialName; }
	public void setCommercialName(String commercialName) { this.commercialName = commercialName; }
	
	private String type;
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }

	private Boolean isNegotiable;
	public Boolean getIsNegotiable() { return isNegotiable; }
	public void setIsNegotiable(Boolean isNegotiable) { this.isNegotiable = isNegotiable; }

	private List<ItemConditionOptionValue> values;
	public List<ItemConditionOptionValue> getValues() {	return values; }
	public void setValues(List<ItemConditionOptionValue> values) { this.values = values; }

}