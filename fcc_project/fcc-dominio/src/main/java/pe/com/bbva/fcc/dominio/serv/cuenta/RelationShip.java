package pe.com.bbva.fcc.dominio.serv.cuenta;

import com.fasterxml.jackson.annotation.JsonInclude;

public class RelationShip {

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	protected Type type;
	public Type getType() { return type; }
	public void setType(Type type) { this.type = type; }
	
}
