package pe.com.bbva.fcc.dominio.serv.contrato;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class FormatCondition {

	protected String id;
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	protected OptionsList unit;
	public OptionsList getUnit() { return unit; }
	public void setUnit(OptionsList unit) { this.unit = unit; }

	protected String pattern;
	public String getPattern() { return pattern; }
	public void setPattern(String pattern) { this.pattern = pattern; }	
}