package pe.com.bbva.fcc.dominio.serv.tarjeta;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Relationship {
	protected String id;
	public String getId() {return id;}
	public void setId(String id) {this.id = id;}
	
	protected Type type;
	public Type getType() {return type;	}	
	public void setType(Type type) {this.type = type;}
	
	protected String order;
	public String getOrder() {return order;}
	public void setOrder(String order) {	this.order = order;	}

}
